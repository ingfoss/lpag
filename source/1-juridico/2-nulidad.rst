================================================
CAPÍTULO II Nulidad de los actos administrativos
================================================

Artículo 8.- Validez del acto administrativo
============================================

Es válido el acto administrativo dictado conforme al ordenamiento jurídico.

(Texto según el artículo 8 de la Ley Nº 27444)

Artículo 9.- Presunción de validez
==================================

Todo acto administrativo se considera válido en tanto su pretendida nulidad no sea declarada por autoridad administrativa o jurisdiccional, según corresponda.

(Texto según el artículo 9 de la Ley Nº 27444)

Artículo 10.- Causales de nulidad
=================================

Son vicios del acto administrativo, que causan su nulidad de pleno derecho, los siguientes:

1. La contravención a la Constitución, a las leyes o a las normas reglamentarias.

2. El defecto o la omisión de alguno de sus requisitos de validez, salvo que se presente alguno de los supuestos de conservación del acto a que se refiere el artículo 14.

3. Los actos expresos o los que resulten como consecuencia de la aprobación automática o por silencio administrativo positivo, por los que se adquiere facultades, o derechos, cuando son contrarios al ordenamiento jurídico, o cuando no se cumplen con los requisitos, documentación o tramites esenciales para su adquisición.

4. Los actos administrativos que sean constitutivos de infracción penal, o que se dicten como consecuencia de la misma.

(Texto según el artículo 10 de la Ley Nº 27444)

Artículo 11.- Instancia competente para declarar la nulidad
===========================================================

11.1 Los administrados plantean la nulidad de los actos administrativos que les conciernan por medio de los recursos administrativos previstos en el Título III Capítulo II de la presente Ley.

11.2 La nulidad de oficio será conocida y declarada por la autoridad superior de quien dictó el acto. Si se tratara de un acto dictado por una autoridad que no está sometida a subordinación jerárquica, la nulidad se declarará por resolución de la misma autoridad.

La nulidad planteada por medio de un recurso de reconsideración o de apelación será conocida y declarada por la autoridad competente para resolverlo.

11.3 La resolución que declara la nulidad dispone, además, lo conveniente para hacer efectiva la responsabilidad del emisor del acto inválido, en los casos en que se advierta ilegalidad manifiesta, cuando sea conocida por el superior jerárquico.

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1272)

Artículo 12.- Efectos de la declaración de nulidad
==================================================

12.1 La declaración de nulidad tendrá efecto declarativo y retroactivo a la fecha del acto, salvo derechos adquiridos de buena fe por terceros, en cuyo caso operará a futuro.

12.2 Respecto del acto declarado nulo, los administrados no están obligados a su cumplimiento y los servidores públicos deberán oponerse a la ejecución del acto, fundando y motivando su negativa.

12.3 En caso de que el acto viciado se hubiera consumado, o bien sea imposible retrotraer sus efectos, sólo dará lugar a la responsabilidad de quien dictó el acto y en su caso, a la indemnización para el afectado.

(Texto según el artículo 12 de la Ley Nº 27444)

Artículo 13.- Alcances de la nulidad
====================================

13.1 La nulidad de un acto sólo implica la de los sucesivos en el procedimiento, cuando estén vinculados a él.

13.2 La nulidad parcial del acto administrativo no alcanza a las otras partes del acto que resulten independientes de la parte nula, salvo que sea su consecuencia, ni impide la producción de efectos para los cuales no obstante el acto pueda ser idóneo, salvo disposición legal en contrario.

13.3 Quien declara la nulidad, dispone la conservación de aquellas actuaciones o trámites cuyo contenido hubiere permanecido igual de no haberse incurrido en el vicio.

(Texto según el artículo 13 de la Ley Nº 27444)

Artículo 14.- Conservación del acto
===================================

14.1 Cuando el vicio del acto administrativo por el incumplimiento a sus elementos de validez, no sea trascendente, prevalece la conservación del acto, procediéndose a su enmienda por la propia autoridad emisora.

14.2 Son actos administrativos afectados por vicios no trascendentes, los siguientes:

14.2.1 El acto cuyo contenido sea impreciso o incongruente con las cuestiones surgidas en la motivación.

14.2.2 El acto emitido con una motivación insuficiente o parcial.

14.2.3 El acto emitido con infracción a las formalidades no esenciales del procedimiento, considerando como tales aquellas cuya realización correcta no hubiera impedido o cambiado el sentido de la decisión final en aspectos importantes, o cuyo incumplimiento no afectare el debido proceso del administrado.

14.2.4 Cuando se concluya indudablemente de cualquier otro modo que el acto administrativo hubiese tenido el mismo contenido, de no haberse producido el vicio.

14.2.5 Aquellos emitidos con omisión de documentación no esencial.

14.3 No obstante la conservación del acto, subsiste la responsabilidad administrativa de quien emite el acto viciado, salvo que la enmienda se produzca sin pedido de parte y antes de su ejecución.

(Texto según el artículo 14 de la Ley Nº 27444)

Artículo 15.- Independencia de los vicios del acto administrativo
=================================================================

Los vicios incurridos en la ejecución de un acto administrativo, o en su notificación a los administrados, son independientes de su validez.

(Texto según el artículo 15 de la Ley Nº 27444)

