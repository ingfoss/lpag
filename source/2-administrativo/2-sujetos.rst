============================================
CAPÍTULO II De los sujetos del procedimiento
============================================

Artículo 61.- Sujetos del procedimiento
=======================================

Para los efectos del cumplimiento de las disposiciones del Derecho Administrativo, se entiende por sujetos del procedimiento a:

1. Administrados: la persona natural o jurídica que, cualquiera sea su calificación o situación procedimental, participa en el procedimiento administrativo. Cuando una entidad interviene en un procedimiento como administrado, se somete a las normas que lo disciplinan en igualdad de facultades y deberes que los demás administrados.

2. Autoridad administrativa: el agente de las entidades que bajo cualquier régimen jurídico, y ejerciendo potestades públicas conducen el inicio, la instrucción, la sustanciación, la resolución, la ejecución, o que de otro modo participan en la gestión de los procedimientos administrativos.

(Texto según el artículo 50 de la Ley Nº 27444)

Subcapítulo I

De los administrados

Artículo 62.- Contenido del concepto administrado
=================================================

Se consideran administrados respecto de algún procedimiento administrativo concreto:

1. Quienes lo promuevan como titulares de derechos o intereses legítimos individuales o colectivos.

2. Aquellos que, sin haber iniciado el procedimiento, posean derechos o intereses legítimos que pueden resultar afectados por la decisión a adoptarse.

(Texto según el artículo 51 de la Ley Nº 27444)

Artículo 63.- Capacidad procesal
================================

Tienen capacidad procesal ante las entidades las personas que gozan de capacidad jurídica conforme a las leyes.

(Texto según el artículo 52 de la Ley Nº 27444)

Artículo 64.- Representación de personas jurídicas
==================================================

Las personas jurídicas pueden intervenir en el procedimiento a través de sus representantes legales, quienes actúan premunidos de los respectivos poderes.

(Texto según el artículo 53 de la Ley Nº 27444)

Artículo 65.- Libertad de actuación procesal
============================================

63.1 El administrado está facultado, en sus relaciones con las entidades, para realizar toda actuación que no le sea expresamente prohibida por algún dispositivo jurídico.

63.2 Para los efectos del numeral anterior, se entiende prohibido todo aquello que impida o perturbe los derechos de otros administrados, o el cumplimiento de sus deberes respecto al procedimiento administrativo.

(Texto según el artículo 54 de la Ley Nº 27444)

Artículo 66.- Derechos de los administrados
===========================================

Son derechos de los administrados con respecto al procedimiento administrativo, los siguientes:

1. La precedencia en la atención del servicio público requerido, guardando riguroso orden de ingreso.

2. Ser tratados con respeto y consideración por el personal de las entidades, en condiciones de igualdad con los demás administrados.

3. Acceder, en cualquier momento, de manera directa y sin limitación alguna a la información contenida en los expedientes de los procedimientos administrativos en que sean partes y a obtener copias de los documentos contenidos en el mismo sufragando el costo que suponga su pedido, salvo las excepciones expresamente previstas por ley.

CONCORDANCIAS:     R.N° 000188-2020-SUNAT (Aprueban disposiciones relativas al servicio de expedición de copias de documentos administrativos, aduaneros y/o tributarios que correspondan a los propios administrados)

4. Acceder a la información gratuita que deben brindar las entidades del Estado sobre sus actividades orientadas a la colectividad, incluyendo sus fines, competencias, funciones, organigramas, ubicación de dependencias, horarios de atención, procedimientos y características.

5. A ser informados en los procedimientos de oficio sobre su naturaleza, alcance y, de ser previsible, del plazo estimado de su duración, así como de sus derechos y obligaciones en el curso de tal actuación.

6. Participar responsable y progresivamente en la prestación y control de los servicios públicos, asegurando su eficiencia y oportunidad.

7. Al cumplimiento de los plazos determinados para cada servicio o actuación y exigirlo así a las autoridades.

8. Ser asistidos por las entidades para el cumplimiento de sus obligaciones.

9. Conocer la identidad de las autoridades y personal al servicio de la entidad bajo cuya responsabilidad son tramitados los procedimientos de su interés.

10. A que las actuaciones de las entidades que les afecten sean llevadas a cabo en la forma menos gravosa posible.

11. Al ejercicio responsable del derecho de formular análisis, críticas o a cuestionar las decisiones y actuaciones de las entidades.

12. A no presentar los documentos prohibidos de solicitar las entidades, a emplear los sucedáneos documentales y a no pagar tasas diferentes a las debidas según las reglas de la presente Ley.

13. A que en caso de renovaciones de autorizaciones, licencias, permisos y similares, se entiendan automáticamente prorrogados en tanto hayan sido solicitados durante la vigencia original, y mientras la autoridad instruye el procedimiento de renovación y notifica la decisión definitiva sobre este expediente.

14. A exigir la responsabilidad de las entidades y del personal a su servicio, cuando así corresponda legalmente, y

15. Los demás derechos reconocidos por la Constitución Política del Perú o las leyes.

(Texto según el artículo 55 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 67.- Deberes generales de los administrados en el procedimiento
========================================================================

Los administrados respecto del procedimiento administrativo, así como quienes participen en él, tienen los siguientes deberes generales:

1. Abstenerse de formular pretensiones o articulaciones ilegales, de declarar hechos contrarios a la verdad o no confirmados como si fueran fehacientes, de solicitar actuaciones meramente dilatorias, o de cualquier otro modo afectar el principio de conducta procedimental

2. Prestar su colaboración para el pertinente esclarecimiento de los hechos.

3. Proporcionar a la autoridad cualquier información dirigida a identificar a otros administrados no comparecientes con interés legítimo en el procedimiento.

4. Comprobar previamente a su presentación ante la entidad, la autenticidad de la documentación sucedánea y de cualquier otra información que se ampare en la presunción de veracidad.

(Texto según el artículo 56 de la Ley Nº 27444)

Artículo 68.- Suministro de información a las entidades
=======================================================

68.1 Los administrados están facultados para proporcionar a las entidades la información y documentos vinculados a sus peticiones o reclamos que estimen necesarios para obtener el pronunciamiento.

68.2 En los procedimientos investigatorios, los administrados están obligados a facilitar la información y documentos que conocieron y fueren razonablemente adecuados a los objetivos de la actuación para alcanzar la verdad material, conforme a lo dispuesto en el capítulo sobre la instrucción.

(Texto según el artículo 57 de la Ley Nº 27444)

Artículo 69.- Comparecencia personal
====================================

69.1 Las entidades pueden convocar la comparecencia personal a su sede de los administrados sólo cuando así le haya sido facultado expresamente por ley.

69.2 Los administrados pueden comparecer asistidos por asesores cuando sea necesario para la mejor exposición de la verdad de los hechos.

69.3 A solicitud verbal del administrado, la entidad entrega al final del acto, constancia de su comparecencia y copia del acta elaborada.

(Texto según el artículo 58 de la Ley Nº 27444)

Artículo 70.- Formalidades de la comparecencia
==============================================

70.1 El citatorio se rige por el régimen común de la notificación, haciendo constar en ella lo siguiente:

70.1.1 El nombre y la dirección del órgano que cita, con identificación de la autoridad requirente;

70.1.2 El objeto y asunto de la comparecencia;

70.1.3 Los nombres y apellidos del citado;

70.1.4 El día y hora en que debe comparecer el citado, que no puede ser antes del tercer día de recibida la citación, y, en caso de ser previsible, la duración máxima que demande su presencia. Convencionalmente puede fijarse el día y hora de comparecencia;

70.1.5 La disposición legal que faculta al órgano a realizar esta citación; y,

70.1.6 El apercibimiento, en caso de inasistencia al requerimiento.

70.2 La comparecencia debe ser realizada, en lo posible, de modo compatible con las obligaciones laborales o profesionales de los convocados.

70.3 El citatorio que infringe alguno de los requisitos indicados no surte efecto, ni obliga a su asistencia a los administrados.

(Texto según el artículo 59 de la Ley Nº 27444)

Artículo 71.- Terceros administrados
====================================

71.1 Si durante la tramitación de un procedimiento es advertida la existencia de terceros determinados no comparecientes cuyos derechos o intereses legítimos puedan resultar afectados con la resolución que sea emitida, dicha tramitación y lo actuado les deben ser comunicados mediante citación al domicilio que resulte conocido, sin interrumpir el procedimiento.

71.2 Respecto de terceros administrados no determinados, la citación es realizada mediante publicación o, cuando corresponda, mediante la realización del trámite de información pública o audiencia pública, conforme a esta Ley.

71.3 Los terceros pueden apersonarse en cualquier estado del procedimiento, teniendo los mismos derechos y obligaciones de los participantes en él.

(Texto según el artículo 60 de la Ley Nº 27444)

Subcapítulo II

De la autoridad administrativa: Principios generales y competencia

Artículo 72.- Fuente de competencia administrativa
==================================================

72.1 La competencia de las entidades tiene su fuente en la Constitución y en la ley, y es reglamentada por las normas administrativas que de aquéllas se derivan.

72.2 Toda entidad es competente para realizar las tareas materiales internas necesarias para el eficiente cumplimiento de su misión y objetivos, así como para la distribución de las atribuciones que se encuentren comprendidas dentro de su competencia.

(Texto según el artículo 61 de la Ley Nº 27444)

Artículo 73.- Presunción de competencia desconcentrada
======================================================

73.1 Cuando una norma atribuya a una entidad alguna competencia o facultad sin especificar qué órgano a su interior debe ejercerla, debe entenderse que corresponde al órgano de inferior jerarquía de función más similar vinculada a ella en razón de la materia y de territorio, y, en caso de existir varios órganos posibles, al superior jerárquico común.

73.2 Particularmente compete a estos órganos resolver los asuntos que consistan en la simple confrontación de hechos con normas expresas o asuntos tales como: certificaciones, inscripciones, remisiones al archivo, notificaciones, expedición de copias certificadas de documentos, comunicaciones o la devolución de documentos.

73.3 Cada entidad es competente para realizar tareas materiales internas necesarias para el eficiente cumplimiento de su misión y objetivos.

(Texto según el artículo 62 de la Ley Nº 27444)

Artículo 74.- Carácter inalienable de la competencia administrativa
===================================================================

74.1 Es nulo todo acto administrativo o contrato que contemple la renuncia a la titularidad, o la abstención del ejercicio de las atribuciones conferidas a algún órgano administrativo.

74.2 Solo por ley o mediante mandato judicial expreso, en un caso concreto, puede ser exigible a una autoridad no ejercer alguna atribución administrativa de su competencia.

74.3 La demora o negligencia en el ejercicio de la competencia o su no ejercicio cuando ello corresponda, constituye falta disciplinaria imputable a la autoridad respectiva.

74.4 Las entidades o sus funcionarios no pueden dejar de cumplir con la tramitación de procedimientos administrativos, conforme a lo normado en la presente Ley. Todo acto en contra es nulo de pleno derecho.

(Texto según el artículo 63 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 75.- Conflicto con la función jurisdiccional
=====================================================

75.1 Cuando, durante la tramitación de un procedimiento, la autoridad administrativa adquiere conocimiento que se está tramitando en sede jurisdiccional una cuestión litigiosa entre dos administrados sobre determinadas relaciones de derecho privado que precisen ser esclarecidas previamente al pronunciamiento administrativo, solicitará al órgano jurisdiccional comunicación sobre las actuaciones realizadas.

75.2 Recibida la comunicación, y sólo si estima que existe estricta identidad de sujetos, hechos y fundamentos, la autoridad competente para la resolución del procedimiento podrá determinar su inhibición hasta que el órgano jurisdiccional resuelva el litigio.

La resolución inhibitoria es elevada en consulta al superior jerárquico, si lo hubiere, aun cuando no medie apelación. Si es confirmada la resolución inhibitoria es comunicada al Procurador Público correspondiente para que, de ser el caso y convenir a los intereses del Estado, se apersone al proceso.

(Texto según el artículo 64 de la Ley Nº 27444)

Artículo 76.- Ejercicio de la competencia
=========================================

76.1 El ejercicio de la competencia es una obligación directa del órgano administrativo que la tenga atribuida como propia, salvo el cambio de competencia por motivos de delegación o evocación, según lo previsto en esta Ley.

76.2 El encargo de gestión, la delegación de firma y la suplencia no suponen alteración de la titularidad de la competencia.

76.3 No puede ser cambiada, alterada o modificada la competencia de las entidades consagradas en la Constitución.

(Texto según el artículo 65 de la Ley Nº 27444)

Artículo 77.- Cambios de competencia por motivos organizacionales
=================================================================

Si durante la tramitación de un procedimiento administrativo, la competencia para conocerlo es transferida a otro órgano o entidad administrativa por motivos organizacionales, en éste continuará el procedimiento sin retrotraer etapas ni suspender plazos.

(Texto según el artículo 66 de la Ley Nº 27444)

Artículo 78.- Delegación de competencia
=======================================

78.1 Las entidades pueden delegar el ejercicio de competencia conferida a sus órganos en otras entidades cuando existan circunstancias de índole técnica, económica, social o territorial que lo hagan conveniente. Procede también la delegación de competencia de un órgano a otro al interior de una misma entidad.

78.2 Son indelegables las atribuciones esenciales del órgano que justifican su existencia, las atribuciones para emitir normas generales, para resolver recursos administrativos en los órganos que hayan dictado los actos objeto de recurso, y las atribuciones a su vez recibidas en delegación.

78.3 Mientras dure la delegación, no podrá el delegante ejercer la competencia que hubiese delegado, salvo los supuestos en que la ley permite la avocación.

78.4 Los actos administrativos emitidos por delegación indican expresamente esta circunstancia y son considerados emitidos por la entidad delegante.

78.5 La delegación se extingue:

a) Por revocación o avocación.

b) Por el cumplimiento del plazo o la condición previstos en el acto de delegación.

(Texto según el artículo 67 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 79.- Deber de vigilancia del delegante
===============================================

El delegante tendrá siempre la obligación de vigilar la gestión del delegado, y podrá ser responsable con éste por culpa en la vigilancia.

(Texto según el artículo 68 de la Ley Nº 27444)

Artículo 80.- Avocación de competencia
======================================

80.1 Con carácter general, la ley puede considerar casos excepcionales de avocación de conocimiento, por parte de los superiores, en razón de la materia, o de la particular estructura de cada entidad.

80.2 La entidad delegante podrá avocarse al conocimiento y decisión de cualquier asunto concreto que corresponda decidir a otra, en virtud de delegación.

(Texto según el artículo 69 de la Ley Nº 27444)

Artículo 81.- Disposición común a la delegación y avocación de competencia
==========================================================================

Todo cambio de competencia debe ser temporal, motivado, y estar su contenido referido a una serie de actos o procedimientos señalados en el acto que lo origina. La decisión que se disponga deberá ser notificada a los administrados comprendidos en el procedimiento en curso con anterioridad a la resolución que se dicte.

(Texto según el artículo 70 de la Ley Nº 27444)

Artículo 82.- Encargo de gestión
================================

82.1 La realización de actividades con carácter material, técnico o de servicios de competencia de un órgano puede ser encargada a otros órganos o entidades por razones de eficacia, o cuando la encargada posea los medios idóneos para su desempeño por sí misma.

82.2 El encargo es formalizado mediante convenio, donde conste la expresa mención de la actividad o actividades a las que afecten el plazo de vigencia, la naturaleza y su alcance.

82.3 El órgano encargante permanece con la titularidad de la competencia y con la responsabilidad por ella, debiendo supervisar la actividad.

82.4 Mediante norma con rango de ley, puede facultarse a las entidades a realizar encargos de gestión a personas jurídicas no estatales, cuando razones de índole técnico y presupuestado lo haga aconsejable bajo los mismos términos previstos en este artículo, dicho encargo deberá realizarse con sujeción al Derecho Administrativo.

(Texto según el artículo 71 de la Ley Nº 27444)

Artículo 83.- Delegación de firma
=================================

83.1 Los titulares de los órganos administrativos pueden delegar mediante comunicación escrita la firma de actos y decisiones de su competencia en sus inmediatos subalternos, o a los titulares de los órganos o unidades administrativas que de ellos dependan, salvo en caso de resoluciones de procedimientos sancionadores, o aquellas que agoten la vía administrativa.

83.2 En caso de delegación de firma, el delegante es el único responsable y el delegado se limita a firmar lo resuelto por aquél.

83.3 El delegado suscribe los actos con la anotación “por”, seguido del nombre y cargo del delegante.

(Texto según el artículo 72 de la Ley Nº 27444)

Artículo 84.- Suplencia
=======================

84.1 El desempeño de los cargos de los titulares de los órganos administrativos puede ser suplido temporalmente en caso de vacancia o ausencia justificada, por quien designe la autoridad competente para efectuar el nombramiento de aquéllos.

84.2 El suplente sustituye al titular para todo efecto legal, ejerciendo las funciones del órgano con la plenitud de los poderes y deberes que las mismas contienen.

84.3 Si no es designado titular o suplente, el cargo es asumido transitoriamente por quien le sigue en jerarquía en dicha unidad; y ante la existencia de más de uno con igual nivel, por quien desempeñe el cargo con mayor vinculación a la gestión del área que suple; y, de persistir la equivalencia, el de mayor antigüedad; en todos los casos con carácter de interino.

(Texto según el artículo 73 de la Ley Nº 27444)

Artículo 85.- Desconcentración
==============================

85.1 La titularidad y el ejercicio de competencia asignada a los órganos administrativos se desconcentran en otros órganos de la entidad, siguiendo los criterios establecidos en la presente Ley.

La desconcentración de competencia puede ser vertical u horizontal. La primera es una forma organizativa de desconcentración de la competencia que se establece en atención al grado y línea del órgano que realiza las funciones, sin tomar en cuenta el aspecto geográfico. La segunda es una forma organizativa de desconcentración de la competencia que se emplea con el objeto de expandir la cobertura de las funciones o servicios administrativos de una entidad.

85.2 Los órganos de dirección de las entidades se encuentran liberados de cualquier rutina de ejecución, de emitir comunicaciones ordinarias y de las tareas de formalización de actos administrativos, con el objeto de que puedan concentrarse en actividades de planeamiento, supervisión, coordinación, control interno de su nivel y en la evaluación de resultados.

85.3 A los órganos jerárquicamente dependientes se les transfiere competencia para emitir resoluciones, con el objeto de aproximar a los administrados las facultades administrativas que conciernan a sus intereses.

85.4 Cuando proceda la impugnación contra actos administrativos emitidos en ejercicio de competencia desconcentrada, corresponderá resolver a quien las haya transferido, salvo disposición legal distinta.

(Texto según el artículo 74 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 86.- Deberes de las autoridades en los procedimientos
==============================================================

Son deberes de las autoridades respecto del procedimiento administrativo y de sus partícipes, los siguientes:

1. Actuar dentro del ámbito de su competencia y conforme a los fines para los que les fueron conferidas sus atribuciones.

2. Desempeñar sus funciones siguiendo los principios del procedimiento administrativo previstos en el Título Preliminar de esta Ley.

3. Encauzar de oficio el procedimiento, cuando advierta cualquier error u omisión de los administrados, sin perjuicio de la actuación que les corresponda a ellos.

4. Abstenerse de exigir a los administrados el cumplimiento de requisitos, la realización de trámites, el suministro de información o la realización de pagos, no previstos legalmente.

5. Realizar las actuaciones a su cargo en tiempo hábil, para facilitar a los administrados el ejercicio oportuno de los actos procedimentales de su cargo.

6. Resolver explícitamente todas las solicitudes presentadas, salvo en aquellos procedimientos de aprobación automática.

7. Velar por la eficacia de las actuaciones procedimentales, procurando la simplificación en sus trámites, sin más formalidades que las esenciales para garantizar el respeto a los derechos de los administrados o para propiciar certeza en las actuaciones.

8. Interpretar las normas administrativas de forma que mejor atienda el fin público al cual se dirigen, preservando razonablemente los derechos de los administrados.

9. Los demás previstos en la presente Ley o derivados del deber de proteger, conservar y brindar asistencia a los derechos de los administrados, con la finalidad de preservar su eficacia.

10. Habilitar espacios idóneos para la consulta de expedientes y documentos, así como para la atención cómoda y ordenada del público, sin perjuicio del uso de medios con aplicación de tecnología de la información u otros similares.

(Texto según el artículo 75 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Subcapítulo III

Colaboración entre entidades

Artículo 87.- Colaboración entre entidades
==========================================

87.1 Las relaciones entre las entidades se rigen por el criterio de colaboración, sin que ello importe renuncia a la competencia propia señalada por ley.

87.2 En atención al criterio de colaboración las entidades deben:

87.2.1 Respetar el ejercicio de competencia de otras entidades, sin cuestionamientos fuera de los niveles institucionales.

87.2.2 Proporcionar directamente los datos e información que posean, sea cual fuere su naturaleza jurídica o posición institucional, a través de cualquier medio, sin más limitación que la establecida por la Constitución o la ley, para lo cual se propenderá a la interconexión de equipos de procesamiento electrónico de información, u otros medios similares.

87.2.3 Prestar en el ámbito propio la cooperación y asistencia activa que otras entidades puedan necesitar para el cumplimiento de sus propias funciones, salvo que les ocasione gastos elevados o ponga en peligro el cumplimiento de sus propias funciones.

87.2.4 Facilitar a las entidades los medios de prueba que se encuentren en su poder, cuando les sean solicitados para el mejor cumplimiento de sus deberes, salvo disposición legal en contrario.

87.2.5 Brindar una respuesta de manera gratuita y oportuna a las solicitudes de información formuladas por otra entidad pública en ejercicio de sus funciones.

87.3 En los procedimientos sujetos a silencio administrativo positivo el plazo para resolver quedará suspendido cuando una entidad requiera la colaboración de otra para que le proporcione la información prevista en los numerales 87.2.3 y 87.2.4, siempre que ésta sea indispensable para la resolución del procedimiento administrativo. El plazo de suspensión no podrá exceder el plazo dispuesto en el numeral 3 del artículo 143.

87.4 Cuando una entidad solicite la colaboración de otra entidad deberá notificar al administrado dentro de los 3 días siguientes de requerida la información.

(Texto según el artículo 76 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 88.- Medios de colaboración interinstitucional
=======================================================

88.1 Las entidades están facultadas para dar estabilidad a la colaboración interinstitucional mediante conferencias entre entidades vinculadas, convenios de colaboración u otros medios legalmente admisibles.

88.2 Las conferencias entre entidades vinculadas permiten a aquellas entidades que correspondan a una misma problemática administrativa, reunirse para intercambiar mecanismos de solución, propiciar la colaboración institucional en aspectos comunes específicos y constituir instancias de cooperación bilateral.

Los acuerdos serán formalizados cuando ello lo amerite, mediante acuerdos suscritos por los representantes autorizados.

88.3. Por los convenios de colaboración, las entidades a través de sus representantes autorizados, celebran dentro de la ley acuerdos en el ámbito de su respectiva competencia, de naturaleza obligatoria para las partes y con cláusula expresa de libre adhesión y separación.

88.4 Las entidades pueden celebrar convenios con las instituciones del sector privado, siempre que con ello se logre el cumplimiento de su finalidad y no se vulnere normas de orden público.

(Texto según el artículo 77 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 89.- Ejecución de la colaboración entre autoridades
============================================================

89.1 La procedencia de la colaboración solicitada es regulada conforme a las normas propias de la autoridad solicitante, pero su cumplimiento es regido por las normas propias de la autoridad solicitada.

89.2 La autoridad solicitante de la colaboración responde exclusivamente por la legalidad de lo solicitado y por el empleo de sus resultados. La autoridad solicitada responde de la ejecución de la colaboración efectuada.

(Texto según el artículo 78 de la Ley Nº 27444)

Artículo 90.- Costas de la colaboración
=======================================

90.1 La solicitud de colaboración no genera el pago de tasas, derechos administrativos o de cualquier otro concepto que implique pago alguno, entre entidades de la administración pública

90.2 A petición de la autoridad solicitada, la autoridad solicitante de otra entidad tendrá que pagar a ésta los gastos efectivos realizados cuando las acciones se encuentren fuera del ámbito de actividad ordinaria de la entidad.

(Texto según el artículo 79 de la Ley Nº 27444)

Subcapítulo IV

Conflictos de competencia y abstención

Artículo 91.- Control de competencia
====================================

Recibida la solicitud o la disposición de autoridad superior, según el caso, para iniciar un procedimiento, las autoridades de oficio deben asegurarse de su propia competencia para proseguir con el normal desarrollo del procedimiento, siguiendo los criterios aplicables al caso de la materia, el territorio, el tiempo, el grado o la cuantía.

(Texto según el artículo 80 de la Ley Nº 27444)

Artículo 92.- Conflictos de competencia
=======================================

92.1 La incompetencia puede ser declarada de oficio, una vez apreciada conforme al artículo anterior o a instancia de los administrados, por el órgano que conoce del asunto o por el superior jerárquico.

92.2 En ningún caso, los niveles inferiores pueden sostener competencia con un superior debiéndole, en todo caso, exponer las razones para su discrepancia.

(Texto según el artículo 81 de la Ley Nº 27444)

Artículo 93.- Declinación de competencia
========================================

93.1 El órgano administrativo que se estime incompetente para la tramitación o resolución de un asunto remite directamente las actuaciones al órgano que considere competente, con conocimiento del administrado.

93.2 El órgano que declina su competencia, a solicitud de parte y hasta antes que otro asuma, puede adoptar las medidas cautelares necesarias para evitar daños graves o irreparables a la entidad o a los administrados, comunicándolo al órgano competente.

(Texto según el artículo 82 de la Ley Nº 27444)

Artículo 94.- Conflicto negativo de competencia
===============================================

En caso de suscitarse conflicto negativo de competencia, el expediente es elevado al órgano inmediato superior para que resuelva el conflicto.

(Texto según el artículo 83 de la Ley Nº 27444)

Artículo 95.- Conflicto positivo de competencia
===============================================

95.1 El órgano que se considere competente requiere de inhibición al que está conociendo del asunto, el cual si está de acuerdo, envía lo actuado a la autoridad requiriente para que continúe el trámite.

95.2 En caso de sostener su competencia la autoridad requerida, remite lo actuado al superior inmediato para que dirima el conflicto.

(Texto según el artículo 84 de la Ley Nº 27444)

CONCORDANCIAS:     R.N° 010-2020-SERVIR-TSC (Precedente administrativo sobre el procedimiento para la determinación de la autoridad que debe intervenir como órgano instructor en caso de discrepancia con la propuesta contenida en el Informe de Precalificación)

Artículo 96.- Resolución de conflicto de competencia
====================================================

En todo conflicto de competencia, el órgano a quien se remite el expediente dicta resolución irrecurrible dentro del plazo de cuatro días.

(Texto según el artículo 85 de la Ley Nº 27444)

Artículo 97.- Competencia para resolver conflictos
==================================================

97.1 Compete resolver los conflictos positivos o negativos de competencia de una misma entidad, al superior jerárquico común, y, si no lo hubiere, al titular de la entidad.

97.2 Los conflictos de competencia entre autoridades de un mismo Sector son resueltos por el responsable de éste, y los conflictos entre otras autoridades del Poder Ejecutivo son resueltos por la Presidencia del Consejo de Ministros, mediante decisión inmotivada; sin ser llevada por las autoridades en ningún caso a los tribunales.

97.3 Los conflictos de competencia entre otras entidades se resuelven conforme a lo que disponen la Constitución y las leyes.

(Texto según el artículo 86 de la Ley Nº 27444)

Artículo 98.- Continuación del procedimiento
============================================

Luego de resuelto el conflicto de competencia, el órgano que resulte competente para conocer el asunto continúa el procedimiento según su estado y conserva todo lo actuado, salvo aquello que no sea jurídicamente posible.

(Texto según el artículo 87 de la Ley Nº 27444)

Artículo 99.- Causales de abstención
====================================

La autoridad que tenga facultad resolutiva o cuyas opiniones sobre el fondo del procedimiento puedan influir en el sentido de la resolución, debe abstenerse de participar en los asuntos cuya competencia le esté atribuida, en los siguientes casos:

1. Si es cónyuge, conviviente, pariente dentro del cuarto grado de consanguinidad o segundo de afinidad, con cualquiera de los administrados o con sus representantes, mandatarios, con los administradores de sus empresas, o con quienes les presten servicios.

2. Si ha tenido intervención como asesor, perito o testigo en el mismo procedimiento, o si como autoridad hubiere manifestado previamente su parecer sobre el mismo, de modo que pudiera entenderse que se ha pronunciado sobre el asunto, salvo la rectificación de errores o la decisión del recurso de reconsideración.

3. Si personalmente, o bien su cónyuge, conviviente o algún pariente dentro del cuarto grado de consanguinidad o segundo de afinidad, tuviere interés en el asunto de que se trate o en otro semejante, cuya resolución pueda influir en la situación de aquel.

4. Cuando tuviere amistad íntima, enemistad manifiesta o conflicto de intereses objetivo con cualquiera de los administrados intervinientes en el procedimiento, que se hagan patentes mediante actitudes o hechos evidentes en el procedimiento.

5. Cuando tuviere o hubiese tenido en los últimos doce (12) meses, relación de servicio o de subordinación con cualquiera de los administrados o terceros directamente interesados en el asunto, o si tuviera en proyecto una concertación de negocios con alguna de las partes, aun cuando no se concrete posteriormente.

No se aplica lo establecido en el presente numeral en los casos de contratos para la prestación de servicios públicos o, que versen sobre operaciones que normalmente realice el administrado-persona jurídica con terceros y, siempre que se acuerden en las condiciones ofrecidas a otros consumidores o usuarios.

6. Cuando se presenten motivos que perturben la función de la autoridad, esta, por decoro, puede abstenerse mediante resolución debidamente fundamentada. Para ello, se debe tener en consideración las siguientes reglas:

a) En caso que la autoridad integre un órgano colegiado, este último debe aceptar o denegar la solicitud.

b) En caso que la autoridad sea un órgano unipersonal, su superior jerárquico debe emitir una resolución aceptando o denegando la solicitud.

(Texto según el artículo 88 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 100.- Promoción de la abstención
=========================================

100.1 La autoridad que se encuentre en alguna de las circunstancias señaladas en el artículo anterior, dentro de los dos (2) días hábiles siguientes a aquel en que comenzó a conocer el asunto, o en que conoció la causal sobreviniente, plantea su abstención en escrito razonado, y remite lo actuado al superior jerárquico inmediato, al presidente del órgano colegiado o al pleno, según el caso, para que sin más trámite, se pronuncie sobre la abstención dentro del tercer día.

100.2 Cuando la autoridad no se abstuviera a pesar de existir alguna de las causales expresadas, el administrado puede hacer conocer dicha situación al titular de la entidad, o al pleno, si fuere órgano colegiado, en cualquier momento.

(Texto según el artículo 89 de la Ley Nº 27444)

Artículo 101.- Disposición superior de abstención
=================================================

101.1 El superior jerárquico inmediato ordena, de oficio, o a pedido de los administrados, la abstención del agente incurso en alguna de las causales a que se refiere el artículo 100.

101.2 En este mismo acto designa a quien continuará conociendo del asunto, preferentemente entre autoridades de igual jerarquía, y le remitirá el expediente.

101.3 Cuando no hubiere otra autoridad pública apta para conocer del asunto, el superior optará por habilitar a una autoridad ad hoc, o disponer que el incurso en causal de abstención tramite y resuelva el asunto, bajo su directa supervisión.

(Texto según el artículo 90 de la Ley Nº 27444)

Artículo 102.- Consecuencias de la no abstención
================================================

102.1 La participación de la autoridad en el que concurra cualquiera de las causales de abstención, no implica necesariamente la invalidez de los actos administrativos en que haya intervenido, salvo en el caso en que resulte evidente la imparcialidad o arbitrariedad manifiesta o que hubiera ocasionado indefensión al administrado.

102.2 Sin perjuicio de ello, el superior jerárquico dispone el inicio de las acciones de responsabilidad administrativa, civil o penal contra la autoridad que no se hubiese abstenido de intervenir, conociendo la existencia de la causal.

(Texto según el artículo 91 de la Ley Nº 27444)

Artículo 103.- Trámite de abstención
====================================

La tramitación de una abstención se realizará en vía incidental, sin suspender los plazos para resolver o para que opere el silencio administrativo.

(Texto según el artículo 92 de la Ley Nº 27444)

Artículo 104.- Impugnación de la decisión
=========================================

La resolución de esta materia no es impugnable en sede administrativa, salvo la posibilidad de alegar la no abstención, como fundamento del recurso administrativo contra la resolución final.

(Texto según el artículo 93 de la Ley Nº 27444)

Artículo 105.- Apartamiento de la autoridad abstenida
=====================================================

La autoridad que por efecto de la abstención sea apartada del procedimiento, coopera para contribuir a la celeridad de la atención del procedimiento, sin participar en reuniones posteriores ni en la deliberación de la decisión.

(Texto según el artículo 94 de la Ley Nº 27444)

Subcapítulo V

Órganos colegiados

Artículo 106.- Régimen de los órganos colegiados
================================================

Se sujetan a las disposiciones del presente apartado, el funcionamiento interno de los órganos colegiados, permanentes o temporales de las entidades, incluidos aquellos en los que participen representantes de organizaciones gremiales, sociales o económicas no estatales.

(Texto según el artículo 95 de la Ley Nº 27444)

Artículo 107.- Autoridades de los órganos colegiados
====================================================

107.1 Cada órgano colegiado de las entidades es representado por un Presidente, a cargo de asegurar la regularidad de las deliberaciones y ejecutar sus acuerdos, y cuenta con un Secretario, a cargo de preparar la agenda, llevar, actualizar y conservar las actas de las sesiones, comunicar los acuerdos, otorgar copias y demás actos propios de la naturaleza del cargo.

107.2 A falta de nominación expresa en la forma prescrita por el ordenamiento, los cargos indicados son elegidos por el propio órgano colegiado entre sus integrantes, por mayoría absoluta de votos.

107.3 En caso de ausencia justificada, pueden ser sustituidos con carácter provisional por los suplentes o, en su defecto, por quien el colegiado elija entre sus miembros.

(Texto según el artículo 96 de la Ley Nº 27444)

Artículo 108.- Atribuciones de los miembros
===========================================

Corresponde a los miembros de los órganos colegiados:

1. Recibir con la antelación prudencial, la convocatoria a las sesiones, con la agenda conteniendo el orden del día y la información suficiente sobre cada tema, de manera que puedan conocer las cuestiones que deban ser debatidas.

2. Participar en los debates de las sesiones.

3. Ejercer su derecho al voto y formular cuando lo considere necesario su voto singular, así como expresar los motivos que lo justifiquen. La fundamentación de un voto singular puede ser realizada en el mismo momento o entregarse por escrito hasta el día siguiente.

4. Formular peticiones de cualquier clase, en particular para incluir temas en la agenda, y formular preguntas durante los debates.

5. Recibir y obtener copia de cualquier documento o acta de las sesiones del órgano colegiado.

(Texto según el artículo 97 de la Ley Nº 27444)

Artículo 109.- Régimen de las sesiones
======================================

109.1 Todo colegiado se reúne ordinariamente con la frecuencia y en el día que indique su ordenamiento; y, a falta de ambos, cuando él lo acuerde.

109.2 La convocatoria de los órganos colegiados corresponde al Presidente y debe ser notificada conjuntamente con la agenda del orden del día con una antelación prudencial, salvo las sesiones de urgencia o periódicas en fecha fija, en que podrá obviarse la convocatoria.

109.3 No obstante, queda válidamente constituido sin cumplir los requisitos de convocatoria u orden del día, cuando se reúnan todos sus miembros y acuerden por unanimidad iniciar la sesión.

109.4 Iniciada la sesión, no puede ser objeto de acuerdo ningún asunto fuera del orden del día, salvo que estén presentes todos los integrantes del órgano colegiado y aprueben mediante su voto unánime la inclusión, en razón a la urgencia de adoptar acuerdo sobre ello.

(Texto según el artículo 98 de la Ley Nº 27444)

Artículo 110.- Quórum para sesiones
===================================

110.1 El quórum para la instalación y sesión válida del órgano colegiado es la mayoría absoluta de sus componentes.

110.2 Si no existiera quórum para la primera sesión, el órgano se constituye en segunda convocatoria el día siguiente de la señalada para la primera, con un quórum de la tercera parte del número legal de sus miembros, y en todo caso, en número no inferior a tres.

110.3 Instalada una sesión, puede ser suspendida sólo por fuerza mayor, con cargo a continuarla en la fecha y lugar que se indique al momento de suspenderla. De no ser posible indicarlo en la misma sesión, la Presidencia convoca la fecha de reinicio notificando a todos los miembros con antelación prudencial.

(Texto según el artículo 99 de la Ley Nº 27444)

Artículo 111.- Quórum para votaciones
=====================================

111.1 Los acuerdos son adoptados por los votos de la mayoría de asistentes al tiempo de la votación en la sesión respectiva, salvo que la ley expresamente establezca una regla distinta; correspondiendo a la Presidencia voto dirimente en caso de empate.

111.2 Los miembros del órgano colegiado que expresen votación distinta a la mayoría deben hacer constar en acta su posición y los motivos que la justifiquen. El Secretario hará constar este voto en el acta junto con la decisión adoptada.

111.3 En caso de órganos colegiados consultivos o informantes, al acuerdo mayoritario se acompaña el voto singular que hubiere.

(Texto según el artículo 100 de la Ley Nº 27444)

Artículo 112.- Obligatoriedad del voto
======================================

112.1 Salvo disposición legal en contrario, los integrantes de órganos colegiados asistentes a la sesión y no impedidos legalmente de intervenir, deben afirmar su posición sobre la propuesta en debate, estando prohibido inhibirse de votar.

112.2 Cuando la abstención de voto sea facultada por ley, tal posición deberá ser fundamentada por escrito.

(Texto según el artículo 101 de la Ley Nº 27444)

Artículo 113.- Acta de sesión
=============================

113.1 De cada sesión es levantada un acta, que contiene la indicación de los asistentes, así como del lugar y tiempo en que ha sido efectuada, los puntos de deliberación, cada acuerdo por separado, con indicación de la forma y sentido de los votos de todos los participantes. El acuerdo expresa claramente el sentido de la decisión adoptada y su fundamento.

113.2 El acta es leída y sometida a la aprobación de los miembros del órgano colegiado al final de la misma sesión o al inicio de la siguiente, pudiendo no obstante el Secretario certificar los acuerdos específicos ya aprobados, así como el pleno autorizar la ejecución inmediata de lo acordado.

113.3 Cada acta, luego de aprobada, es firmada por el Secretario, el Presidente, por quienes hayan votado singularmente y por quienes así lo soliciten.

(Texto según el artículo 102 de la Ley Nº 27444)

