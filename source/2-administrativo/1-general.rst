TÍTULO II Del procedimiento administrativo

==================================
CAPÍTULO I Disposiciones Generales
==================================

Artículo 29.- Definición de procedimiento administrativo
========================================================

Se entiende por procedimiento administrativo al conjunto de actos y diligencias tramitados en las entidades, conducentes a la emisión de un acto administrativo que produzca efectos jurídicos individuales o individualizables sobre intereses, obligaciones o derechos de los administrados.

(Texto según el artículo 29 de la Ley Nº 27444)

Artículo 30.- Procedimiento Administrativo Electrónico
======================================================

30.1 Sin perjuicio del uso de medios físicos tradicionales, el procedimiento administrativo podrá realizarse total o parcialmente a través de tecnologías y medios electrónicos, debiendo constar en un expediente, escrito electrónico, que contenga los documentos presentados por los administrados, por terceros y por otras entidades, así como aquellos documentos remitidos al administrado.

30.2 El procedimiento administrativo electrónico deberá respetar todos los principios, derechos y garantías del debido procedimiento previstos en la presente Ley, sin que se afecte el derecho de defensa ni la igualdad de las partes, debiendo prever las medidas pertinentes cuando el administrado no tenga acceso a medios electrónicos.

30.3 Los actos administrativos realizados a través del medio electrónico, poseen la misma validez y eficacia jurídica que los actos realizados por medios físicos tradicionales. Las firmas digitales y documentos generados y procesados a través de tecnologías y medios electrónicos, siguiendo los procedimientos definidos por la autoridad administrativa, tendrán la misma validez legal que los documentos manuscritos.

30.4 Mediante Decreto Supremo, refrendado por la Presidencia del Consejo de Ministros, se aprueban lineamientos para establecer las condiciones y uso de las tecnologías y medios electrónicos en los procedimientos administrativos, junto a sus requisitos.

(Artículo incorporado por el artículo 4 del Decreto Legislativo Nº 1272)

Artículo 31.- Expediente Electrónico
====================================

31.1 El expediente electrónico está constituido por el conjunto de documentos electrónicos generados a partir de la iniciación del procedimiento administrativo o servicio prestado en exclusividad en una determinada entidad de la Administración Pública.

31.2 El expediente electrónico debe tener un número de identificación único e inalterable que permita su identificación unívoca dentro de la entidad que lo origine. Dicho número permite, a su vez, su identificación para efectos de un intercambio de información entre entidades o por partes interesadas, así como para la obtención de copias del mismo en caso corresponda.

31.3 Cada documento electrónico incorporado en el expediente electrónico debe ser numerado correlativamente, de modo que se origine un índice digital el cual es firmado electrónicamente conforme a ley por el personal responsable de la entidad de la Administración Pública a fin de garantizar la integridad y su recuperación siempre que sea preciso.

(Artículo incorporado según el artículo 3 del Decreto Legislativo Nº 1452)

Artículo 32.- Calificación de procedimientos administrativos
============================================================

Todos los procedimientos administrativos que, por exigencia legal, deben iniciar los administrados ante las entidades para satisfacer o ejercer sus intereses o derechos, se clasifican conforme a las disposiciones del presente capítulo, en: procedimientos de aprobación automática o de evaluación previa por la entidad, y este último a su vez sujeto, en caso de falta de pronunciamiento oportuno, a silencio positivo o silencio negativo. Cada entidad señala estos procedimientos en su Texto Único de Procedimientos Administrativos - TUPA, siguiendo los criterios establecidos en el presente ordenamiento.

(Texto según el artículo 30 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1272)

Artículo 33.- Régimen del procedimiento de aprobación automática
================================================================

33.1 En el procedimiento de aprobación automática, la solicitud es considerada aprobada desde el mismo momento de su presentación ante la entidad competente para conocerla, siempre que cumpla con los requisitos y entregue la documentación completa, exigidos en el TUPA de la entidad.

33.2 En este procedimiento, las entidades no emiten ningún pronunciamiento expreso confirmatorio de la aprobación automática, debiendo sólo realizar la fiscalización posterior. Sin embargo, cuando en los procedimientos de aprobación automática se requiera necesariamente de la expedición de un documento sin el cual el usuario no puede hacer efectivo su derecho, el plazo máximo para su expedición es de cinco días hábiles, sin perjuicio de aquellos plazos mayores fijados por leyes especiales anteriores a la vigencia de la presente Ley.

33.3 Como constancia de la aprobación automática de la solicitud del administrado, basta la copia del escrito o del formato presentado conteniendo el sello oficial de recepción, sin observaciones e indicando el número de registro de la solicitud, fecha, hora y firma del agente receptor.

33.4 Son procedimientos de aprobación automática, sujetos a la presunción de veracidad, aquellos que habiliten el ejercicio de derechos preexistentes del administrado, la inscripción en registros administrativos, la obtención de licencias, autorizaciones, constancias y copias certificadas o similares que habiliten para el ejercicio continuado de actividades profesionales, sociales, económicas o laborales en el ámbito privado, siempre que no afecten derechos de terceros y sin perjuicio de la fiscalización posterior que realice la administración.

33.5 La Presidencia del Consejo de Ministros se encuentra facultada para determinar los procedimientos sujetos a aprobación automática. Dicha calificación es de obligatoria adopción, a partir del día siguiente de su publicación en el diario oficial, sin necesidad de actualización previa del Texto Único de Procedimientos Administrativos por las entidades, sin perjuicio de lo establecido en el numeral 44.7 del artículo 44.

(Texto según el artículo 31 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1272)

Artículo 34.- Fiscalización posterior
=====================================

34.1 Por la fiscalización posterior, la entidad ante la que es realizado un procedimiento de aprobación automática, evaluación previa o haya recibido la documentación a que se refiere el artículo 49; queda obligada a verificar de oficio mediante el sistema del muestreo, la autenticidad de las declaraciones, de los documentos, de las informaciones y de las traducciones proporcionadas por el administrado.

34.2 Tratándose de los procedimientos de aprobación automática y en los de evaluación previa en los que ha operado el silencio administrativo positivo, la fiscalización comprende no menos del diez por ciento (10%) de todos los expedientes, con un máximo de ciento cincuenta (150) expedientes por semestre. Esta cantidad puede incrementarse teniendo en cuenta el impacto que en el interés general, en la economía, en la seguridad o en la salud ciudadana pueda conllevar la ocurrencia de fraude o falsedad en la información, documentación o declaración presentadas. Dicha fiscalización debe efectuarse semestralmente de acuerdo a los lineamientos que para tal efecto dicta la Presidencia del Consejo de Ministros.

34.3 En caso de comprobar fraude o falsedad en la declaración, información o en la documentación presentada por el administrado, la entidad considerará no satisfecha la exigencia respectiva para todos sus efectos, procediendo a declarar la nulidad del acto administrativo sustentado en dicha declaración, información o documento; e imponer a quien haya empleado esa declaración, información o documento una multa en favor de la entidad de entre cinco (5) y diez (10) Unidades Impositivas Tributarias vigentes a la fecha de pago; y, además, si la conducta se adecua a los supuestos previstos en el Título XIX Delitos contra la Fe Pública del Código Penal, ésta deberá ser comunicada al Ministerio Público para que interponga la acción penal correspondiente.

34.4 Como resultado de la fiscalización posterior, la relación de administrados que hubieren presentado declaraciones, información o documentos falsos o fraudulentos al amparo de procedimientos de aprobación automática y de evaluación previa, es publicada trimestralmente por la Central de Riesgo Administrativo, a cargo de la Presidencia del Consejo de Ministros, consignando el Documento Nacional de Identidad o el Registro Único de Contribuyente y la dependencia ante la cual presentaron dicha información. Las entidades deben elaborar y remitir la indicada relación a la Central de Riesgo Administrativo, siguiendo los lineamientos vigentes sobre la materia. Las entidades están obligadas a incluir de manera automática en sus acciones de fiscalización posterior todos los procedimientos iniciados por los administrados incluidos en la relación de Central de Riesgo Administrativo.

(Texto según el artículo 32 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1272)

Artículo 35.- Procedimiento de evaluación previa con silencio positivo
======================================================================

35.1 Los procedimientos de evaluación previa están sujetos a silencio positivo, cuando se trate de algunos de los siguientes supuestos:

1.- Todos los procedimientos a instancia de parte no sujetos al silencio administrativo negativo taxativo contemplado en el artículo 38.

2.- Recursos destinados a cuestionar la desestimación de una solicitud cuando el particular haya optado por la aplicación del silencio administrativo negativo.

35.2 Como constancia de la aplicación del silencio positivo de la solicitud del administrado, basta la copia del escrito o del formato presentado conteniendo el sello oficial de recepción, sin observaciones e indicando el número de registro de la solicitud, fecha, hora y firma del agente receptor. En el caso de procedimientos administrativos electrónicos, basta el correo electrónico que deja constancia del envío de la solicitud.

35.3 La Presidencia del Consejo de Ministros se encuentra facultada para determinar los procedimientos sujetos a silencio positivo. Dicha calificación será de obligatoria adopción, a partir del día siguiente de su publicación en el diario oficial, sin necesidad de actualización previa del Texto Único de Procedimientos Administrativos por las entidades, sin perjuicio de lo establecido en el numeral 44.7 del artículo 44.

35.4 Los procedimientos de petición graciable y de consulta se rigen por su regulación específica.

(Artículo incorporado por el artículo 4 del Decreto Legislativo Nº 1272)

Artículo 36.- Aprobación de petición mediante el silencio positivo
==================================================================

36.1 En los procedimientos administrativos sujetos a silencio positivo, la petición del administrado se considera aprobada si, vencido el plazo establecido o máximo para pronunciarse, la entidad no hubiera notificado el pronunciamiento correspondiente, no siendo necesario expedirse pronunciamiento o documento alguno para que el administrado pueda hacer efectivo su derecho, bajo responsabilidad del funcionario o servidor público que lo requiera.

36.2 Lo dispuesto en el presente artículo no enerva la obligación de la entidad de realizar la fiscalización posterior de los documentos, declaraciones e información presentados por el administrado, conforme a lo dispuesto en el artículo 34.

(Artículo incorporado por el artículo 4 del Decreto Legislativo Nº 1272)

Artículo 37.- Aprobación del procedimiento.
===========================================

37.1 No obstante lo señalado en el artículo 36, vencido el plazo para que opere el silencio positivo en los procedimientos de evaluación previa, regulados en el artículo 35, sin que la entidad hubiera emitido pronunciamiento sobre lo solicitado, los administrados, si lo consideran pertinente y de manera complementaria, pueden presentar una Declaración Jurada ante la propia entidad que configuró dicha aprobación ficta, con la finalidad de hacer valer el derecho conferido ante la misma o terceras entidades de la administración, constituyendo el cargo de recepción de dicho documento, prueba suficiente de la resolución aprobatoria ficta de la solicitud o trámite iniciado.

37.2 Lo dispuesto en el párrafo anterior es aplicable también al procedimiento de aprobación automática, reemplazando la aprobación ficta, contenida en la Declaración Jurada, al documento a que hace referencia el numeral 33.2 del artículo 33.

37.3 En el caso que la autoridad administrativa se niegue a recibir la Declaración Jurada a que se refiere el párrafo anterior, el administrado puede remitirla por conducto notarial, surtiendo los mismos efectos.

(Artículo incorporado por el artículo 4 del Decreto Legislativo Nº 1272)

Artículo 38.- Procedimientos de evaluación previa con silencio negativo.
========================================================================

38.1 Excepcionalmente, el silencio negativo es aplicable en aquellos casos en los que la petición del administrado puede afectar significativamente el interés público e incida en los siguientes bienes jurídicos: la salud, el medio ambiente, los recursos naturales, la seguridad ciudadana, el sistema financiero y de seguros, el mercado de valores, la defensa comercial, la defensa nacional y el patrimonio cultural de la nación, así como en aquellos procedimientos de promoción de inversión privada, procedimientos trilaterales, procedimientos de inscripción registral y en los que generen obligación de dar o hacer del Estado y autorizaciones para operar casinos de juego y máquinas tragamonedas.

La calificación excepcional del silencio negativo se produce en la norma de creación o modificación del procedimiento administrativo, debiendo sustentar técnica y legalmente su calificación en la exposición de motivos, en la que debe precisarse la afectación en el interés público y la incidencia en alguno de los bienes jurídicos previstos en el párrafo anterior.

Por Decreto Supremo, refrendado por el Presidente del Consejo de Ministros, se puede ampliar las materias en las que, por afectar significativamente el interés público, corresponde la aplicación de silencio administrativo negativo.

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1452)

38.2 Asimismo, es de aplicación para aquellos procedimientos por los cuales se transfiera facultades de la administración pública.

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1452)

38.3 En materia tributaria y aduanera, el silencio administrativo se rige por sus leyes y normas especiales. Tratándose de procedimientos administrativos que tengan incidencia en la determinación de la obligación tributaria o aduanera, se aplica el Código Tributario.

38.4 Las autoridades quedan facultadas para calificar de modo distinto en su Texto Único de Procedimientos Administrativos los procedimientos administrativos señalados, con excepción de los procedimientos trilaterales y en los que generen obligación de dar o hacer del Estado, cuando aprecien que sus efectos reconozcan el interés del solicitante, sin exponer significativamente el interés general.

(Artículo incorporado por el artículo 4 del Decreto Legislativo Nº 1272)

Artículo 39.- Plazo máximo del procedimiento administrativo de evaluación previa
================================================================================

El plazo que transcurra desde el inicio de un procedimiento administrativo de evaluación previa hasta que sea dictada la resolución respectiva, no puede exceder de treinta (30) días hábiles, salvo que por ley o decreto legislativo se establezcan procedimientos cuyo cumplimiento requiera una duración mayor.

(Texto según el artículo 35 de la Ley Nº 27444)

Artículo 40.- Legalidad del procedimiento
=========================================

40.1 Los procedimientos administrativos y requisitos deben establecerse en una disposición sustantiva aprobada mediante decreto supremo o norma de mayor jerarquía, por Ordenanza Regional, por Ordenanza Municipal, por Resolución del titular de los organismos constitucionalmente autónomos.

En el caso de los organismos reguladores estos podrán establecer procedimientos y requisitos en ejercicio de su función normativa.

Los organismos técnicos especializados del Poder Ejecutivo pueden establecer procedimientos administrativos y requisitos mediante resolución del órgano de dirección o del titular de la entidad, según corresponda, para lo cual deben estar habilitados por ley o decreto legislativo a normar el otorgamiento o reconocimiento de derechos de los particulares, el ingreso a mercados o el desarrollo de actividades económicas. El establecimiento de los procedimientos y requisitos debe cumplir lo dispuesto en el presente numeral y encontrarse en el marco de lo dispuesto en las políticas, planes y lineamientos del sector correspondiente.

40.2 Las entidades realizan el Análisis de Calidad Regulatoria de los procedimientos administrativos a su cargo o sus propuestas, teniendo en cuenta el alcance establecido en la normativa vigente sobre la materia.

40.3 Los procedimientos administrativos deben ser compendiados y sistematizados en el Texto Único de Procedimientos Administrativos, aprobados para cada entidad, en el cual no se pueden crear procedimientos ni establecer nuevos requisitos, salvo lo relativo a la determinación de los derechos de tramitación que sean aplicables de acuerdo a la normatividad vigente.(*)

(*) De conformidad con la Primera Disposición Complementaria Final del Decreto Legislativo N° 1477, publicado el 08 mayo 2020, se autoriza a las Municipalidades competentes a aplicar y atender el procedimiento administrativo especial establecido en los artículos 3, 4 y 5 del citado Decreto Legislativo, desde el día siguiente de su publicación, quedando exentas de lo establecido en el presente numeral.

40.4 Las entidades solamente exigen a los administrados el cumplimiento de procedimientos, la presentación de documentos, el suministro de información o el pago por derechos de tramitación, siempre que cumplan con los requisitos previstos en el numeral anterior. Incurre en responsabilidad la autoridad que procede de modo diferente, realizando exigencias a los administrados fuera de estos casos.

40.5 Las disposiciones concernientes a la eliminación de procedimientos o requisitos o a la simplificación de los mismos pueden aprobarse por Resolución Ministerial, por Resolución de Consejo Directivo de los Organismos Reguladores, Resolución del órgano de dirección o del titular de los organismos técnicos especializados, según corresponda, Resolución del titular de los organismos constitucionalmente autónomos, Decreto Regional o Decreto de Alcaldía, según se trate de entidades dependientes del Poder Ejecutivo, Organismos Constitucionalmente Autónomos, Gobiernos Regionales o Locales, respectivamente.

40.6 Los procedimientos administrativos, incluyendo sus requisitos, a cargo de las personas jurídicas bajo el régimen privado que prestan servicios públicos o ejercen función administrativa deben ser debidamente publicitados, para conocimiento de los administrados.

(Texto según el artículo 36 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1452)

Artículo 41.- Procedimientos Administrativos estandarizados obligatorios.
=========================================================================

41.1 Mediante decreto supremo refrendado por la Presidencia del Consejo de Ministros se aprueban procedimientos administrativos y servicios prestados en exclusividad estandarizados de obligatoria aplicación por las entidades competentes para tramitarlos, las que no están facultadas para modificarlos o alterarlos. Las entidades están obligadas a incorporar dichos procedimientos y servicios estandarizados en su respectivo Texto Único de Procedimientos Administrativos sin necesidad de aprobación por parte de otra entidad. Las entidades solo podrán determinar: la unidad de trámite documentario o la que haga sus veces para dar inicio al procedimiento administrativo o servicio prestado en exclusividad, la autoridad competente para resolver el procedimiento administrativo y la unidad orgánica a la que pertenece, y la autoridad competente que resuelve los recursos administrativos, en lo que resulte pertinente.

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1452)

CONCORDANCIAS:     D.S.N° 045-2019-PCM, Art. 5 (Adecuación de los TUPA de las municipalidades)

41.2 La no actualización por las entidades de sus respectivos Texto Único de Procedimiento Administrativo dentro de los cinco (5) días hábiles posteriores a la entrada en vigencia de los procedimientos administrativos estandarizados por la Presidencia del Consejo de Ministros, tiene como consecuencia la aplicación del artículo 58.

(Artículo incorporado por el artículo 4 del Decreto Legislativo Nº 1272)

Artículo 42.- Vigencia indeterminada de los títulos habilitantes
================================================================

Los títulos habilitantes emitidos tienen vigencia indeterminada, salvo que por ley o decreto legislativo se establezca un plazo determinado de vigencia. Cuando la autoridad compruebe el cambio de las condiciones indispensables para su obtención, previa fiscalización, podrá dejar sin efecto el título habilitante.

Excepcionalmente, por decreto supremo, se establece la vigencia determinada de los títulos habilitantes, para lo cual la entidad debe sustentar la necesidad, el interés público a tutelar y otros criterios que se definan de acuerdo a la normativa de calidad regulatoria.

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1452)

Artículo 43. Contenido del Texto Único de Procedimientos Administrativos
========================================================================

43.1 Todas las entidades elaboran y aprueban o gestionan la aprobación, según el caso, de su Texto Único de Procedimientos Administrativos, el cual comprende:

1. Todos los procedimientos de iniciativa de parte requeridos por los administrados para satisfacer sus intereses o derechos mediante el pronunciamiento de cualquier órgano de la entidad, siempre que esa exigencia cuente con respaldo legal, el cual deberá consignarse expresamente en el TUPA con indicación de la fecha de publicación en el Diario Oficial.

2. La descripción clara y taxativa de todos los requisitos exigidos para la realización completa de cada procedimiento, los cuales deben ser establecidos conforme a lo previsto en el numeral anterior.

3. La calificación de cada procedimiento según corresponda entre procedimientos de evaluación previa o de aprobación automática.

4. En el caso de procedimientos de evaluación previa si el silencio administrativo aplicable es negativo o positivo.

5. Los supuestos en que procede el pago de derechos de tramitación, con indicación de su monto y forma de pago. El monto de los derechos se expresa publicándose en la entidad en moneda de curso legal.

6. Las vías de recepción adecuadas para acceder a los procedimientos contenidos en los TUPA, de acuerdo a lo dispuesto por los artículos 127 y siguientes.

7. La autoridad competente para resolver en cada instancia del procedimiento y los recursos a interponerse para acceder a ellas.

8. Los formularios que sean empleados durante la tramitación del respectivo procedimiento administrativo, no debiendo emplearse para la exigencia de requisitos adicionales.

La información complementaria como sedes de atención, horarios, medios de pago, datos de contacto, notas al ciudadano; su actualización es responsabilidad de la máxima autoridad administrativa de la entidad que gestiona el TUPA, sin seguir las formalidades previstas en los numerales 44.1 o 44.5.

La Presidencia del Consejo de Ministros, mediante Resolución de la Secretaría de Gestión Pública, aprueba el Formato del Texto Único de Procedimientos Administrativos aplicable para las entidades previstas en los numerales 1 al 7 del artículo I del Título Preliminar de la presente ley.

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1452)

43.2 El TUPA también incluye la relación de los servicios prestados en exclusividad, entendidos como las prestaciones que las entidades se encuentran facultadas a brindar en forma exclusiva en el marco de su competencia, no pudiendo ser realizadas por otra entidad o terceros. Son incluidos en el TUPA, resultando aplicable lo previsto en los numerales 2, 5, 6, 7 y 8 del numeral anterior, en lo que fuera aplicable.

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1452)

43.3 Los requisitos y condiciones para la prestación de los servicios brindados en exclusividad por las entidades son fijados por decreto supremo refrendado por el Presidente del Consejo de Ministros.

43.4 Para aquellos servicios que no sean prestados en exclusividad, las entidades, a través de Resolución del Titular de la entidad establecen la denominación, la descripción clara y taxativa de los requisitos y sus respectivos costos, los cuales deben ser debidamente difundidos para que sean de público conocimiento, respetando lo establecido en el artículo 60 de la Constitución Política del Perú y las normas sobre represión de la competencia desleal.

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1452)

(Texto según el artículo 37 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1272)

Artículo 44.- Aprobación y difusión del Texto Único de Procedimientos Administrativos
=====================================================================================

44.1 El Texto Único de Procedimientos Administrativos (TUPA) es aprobado por Decreto Supremo del sector, por Ordenanza Regional, por Ordenanza Municipal, o por Resolución del Titular de organismo constitucionalmente autónomo, según el nivel de gobierno respectivo.

(Texto según el numeral 38.1 del artículo 38 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1452)

44.2. La norma que aprueba el TUPA se publica en el diario oficial El Peruano.

44.3 El TUPA y la disposición legal de aprobación o modificación se publica obligatoriamente en el portal del diario oficial El Peruano. Adicionalmente se difunde a través de la Plataforma Digital Única para Orientación al Ciudadano del Estado Peruano y en el respectivo Portal Institucional de la entidad. La publicación en los medios previstos en el presente numeral se realiza de forma gratuita.

(Texto según el numeral 38.3 del artículo 38 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1452)

44.4 Sin perjuicio de la indicada publicación, cada entidad realiza la difusión de su TUPA mediante su ubicación en lugar visible de la entidad.

44.5 Una vez aprobado el TUPA, toda modificación que no implique la creación de nuevos procedimientos, incremento de derechos de tramitación o requisitos, se debe realizar por Resolución Ministerial del Sector, o por resolución del titular del Organismo Autónomo conforme a la Constitución Política del Perú, o por Resolución de Consejo Directivo de los Organismos Reguladores, Resolución del órgano de dirección o del titular de los organismos técnicos especializados, según corresponda, Decreto Regional o Decreto de Alcaldía, según el nivel de gobierno respectivo. En caso contrario, su aprobación se realiza conforme al mecanismo establecido en el numeral 44.1. En ambos casos se publicará la modificación según lo dispuesto por los numerales 44.2 y 44.3.

(Texto según el numeral 38.5 del artículo 38 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1452)

44.6 Para la elaboración del TUPA se evita la duplicidad de procedimientos administrativos en las entidades.

44.7 En los casos en que por Ley, Decreto Legislativo y demás normas de alcance general, se establezcan o se modifiquen los requisitos, plazo o silencio administrativo aplicables a los procedimientos administrativos, las entidades de la Administración Pública están obligadas a realizar las modificaciones correspondientes en sus respectivos Textos Únicos de Procedimientos Administrativos en un plazo máximo de sesenta (60) días hábiles, contados a partir de la entrada en vigencia de la norma que establece o modifica los requisitos, plazo o silencio administrativo aplicables a los procedimientos administrativos. Si vencido dicho plazo, la entidad no ha actualizado el TUPA incorporando el procedimiento establecido o modificado en la normatividad vigente, no puede dejar de emitir pronunciamiento respecto al procedimiento o prestar el servicio que se encuentre vigente de acuerdo al marco legal correspondiente, bajo responsabilidad.(*)

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1452)

(*) De conformidad con la Primera Disposición Complementaria Final del Decreto Legislativo N° 1477, publicado el 08 mayo 2020, se autoriza a las Municipalidades competentes a aplicar y atender el procedimiento administrativo especial establecido en los artículos 3, 4 y 5 del citado Decreto Legislativo, desde el día siguiente de su publicación, quedando exentas de lo establecido en el presente numeral.

44.8 Incurre en responsabilidad administrativa el funcionario que:

a) Solicita o exige el cumplimiento de requisitos que no están en el TUPA o que, estando en el TUPA, no han sido establecidos por la normatividad vigente o han sido derogados.

b) Aplique tasas que no han sido aprobadas conforme a lo dispuesto por los artículos 53 y 54, y por el Texto Único Ordenado del Código Tributario, cuando corresponda.

c) Aplique tasas que no han sido ratificadas por la Municipalidad Provincial correspondiente, conforme a las disposiciones establecidas en el artículo 40 de la Ley 27972, Ley Orgánica de Municipalidades.

Asimismo, incurre en responsabilidad administrativa el Alcalde y el gerente municipal, o quienes hagan sus veces, cuando transcurrido el plazo de cuarenta y cinco (45) días hábiles luego de recibida la solicitud de ratificación de la municipalidad distrital, no haya cumplido con atender la solicitud de ratificación de las tasas a las que se refiere el artículo 40 de la Ley 27972, Ley Orgánica de Municipalidades, salvo las tasas por arbitrios en cuyo caso el plazo será de sesenta (60) días hábiles. (*) RECTIFICADO POR FE DE ERRATAS

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1452)

Sin perjuicio de lo anterior, las exigencias establecidas en los literales precedentes, también constituyen barrera burocrática ilegal, siendo aplicables las sanciones establecidas en el Decreto Legislativo Nº 1256, que aprueba la Ley de Prevención y Eliminación de Barreras Burocráticas o norma que lo sustituya.

44.9 La Contraloría General de la República, en el marco de la Ley Orgánica del Sistema Nacional de Control y de la Contraloría General de la República, verifica el cumplimiento de los plazos señalados en el numeral 44.7 del presente artículo.

(Texto modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 45.- Consideraciones para estructurar el procedimiento
===============================================================

45.1 Solamente serán incluidos como requisitos exigidos para la realización de cada procedimiento administrativo aquellos que razonablemente sean indispensables para obtener el pronunciamiento correspondiente, atendiendo además a sus costos y beneficios.

45.2 Para tal efecto, cada entidad considera como criterios:

45.2.1 La documentación que conforme a esta ley pueda ser solicitada, la impedida de requerir y aquellos sucedáneos establecidos en reemplazo de documentación original.

45.2.2 Su necesidad y relevancia en relación al objeto del procedimiento administrativo y para obtener el pronunciamiento requerido.

45.2.3 La capacidad real de la entidad para procesar la información exigida, en vía de evaluación previa o fiscalización posterior.

(Texto según el artículo 39 de la Ley Nº 27444)

Artículo 46.- Acceso a información para consulta por parte de las entidades
===========================================================================

46.1 Todas las entidades tienen la obligación de permitir a otras, gratuitamente, el acceso a sus bases de datos y registros para consultar sobre información requerida para el cumplimiento de requisitos de procedimientos administrativos o servicios prestados en exclusividad.

46.2 En estos casos, la entidad únicamente solicita al administrado la presentación de una declaración jurada en el cual manifieste que cumple con el requisito previsto en el procedimiento administrativo o servicio prestado en exclusividad.

(Artículo incorporado por el artículo 4 del Decreto Legislativo Nº 1272)

Artículo 47.- Enfoque intercultural
===================================

Las autoridades administrativas deben actuar aplicando un enfoque intercultural, coadyuvando a la generación de un servicio con pertinencia cultural, lo que implica la adaptación de los procesos que sean necesarios en función a las características geográficas, ambientales, socioeconómicas, lingüísticas y culturales de los administrados a quienes se destina dicho servicio.

(Artículo incorporado según el artículo 3 del Decreto Legislativo Nº 1452)

Artículo 48.- Documentación prohibida de solicitar
==================================================

48.1 Para el inicio, prosecución o conclusión de todo procedimiento, común o especial, las entidades quedan prohibidas de solicitar a los administrados la presentación de la siguiente información o la documentación que la contenga:

48.1.1 Aquella que la entidad solicitante genere o posea como producto del ejercicio de sus funciones públicas conferidas por la Ley o que deba poseer en virtud de algún trámite realizado anteriormente por el administrado en cualquiera de sus dependencias, o por haber sido fiscalizado por ellas, durante cinco (5) años anteriores inmediatos, siempre que los datos no hubieren sufrido variación. Para acreditarlo, basta que el administrado exhiba la copia del cargo donde conste dicha presentación, debidamente sellado y fechado por la entidad ante la cual hubiese sido suministrada.

48.1.2 Aquella que haya sido expedida por la misma entidad o por otras entidades públicas del sector, en cuyo caso corresponde a la propia entidad recabarla directamente.

48.1.3 Presentación de más de dos ejemplares de un mismo documento ante la entidad, salvo que sea necesario notificar a otros tantos interesados.

48.1.4 Fotografías personales, salvo para obtener documentos de identidad, pasaporte o licencias o autorizaciones de índole personal, por razones de seguridad nacional y seguridad ciudadana. Los administrados suministrarán ellos mismos las fotografías solicitadas o tendrán libertad para escoger la empresa que las produce, con excepción de los casos de digitalización de imágenes.

48.1.5 Documentos de identidad personal distintos al Documento Nacional de Identidad. Asimismo, solo se exigirá para los ciudadanos extranjeros carné de extranjería o pasaporte según corresponda.

48.1.6 Recabar sellos de la propia entidad, que deben ser acopiados por la autoridad a cargo del expediente.

48.1.7 Documentos o copias nuevas, cuando sean presentadas otras, no obstante haber sido producidos para otra finalidad, salvo que sean ilegibles.

48.1.8 Constancia de pago realizado ante la propia entidad por algún trámite, en cuyo caso el administrado sólo queda obligado a informar en su escrito el día de pago y el número de constancia de pago, correspondiendo a la administración la verificación inmediata.

48.1.9 Aquella que, de conformidad con la normativa aplicable, se acreditó o debió acreditarse en una fase anterior o para obtener la culminación de un trámite anterior ya satisfecho. En este supuesto, la información o documentación se entenderá acreditada para todos los efectos legales.

48.1.10 Toda aquella información o documentación que las entidades de la Administración Pública administren, recaben, sistematicen, creen o posean respecto de los usuarios o administrados que están obligadas a suministrar o poner a disposición de las demás entidades que las requieran para la tramitación de sus procedimientos administrativos y para sus actos de administración interna, de conformidad con lo dispuesto por ley, decreto legislativo o por Decreto Supremo refrendado por el Presidente del Consejo de Ministros.

Los plazos y demás condiciones para la aplicación de lo dispuesto en el presente numeral a entidades de la Administración Pública distintas del Poder Ejecutivo, son establecidos mediante Decreto Supremo refrendado por el Presidente del Consejo de Ministros.

48.2 Las disposiciones contenidas en este artículo no limitan la facultad del administrado para presentar espontáneamente la documentación mencionada, de considerarlo conveniente.

(Texto según el artículo 40 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1272)

Artículo 49.- Presentación de documentos sucedáneos de los originales
=====================================================================

49.1 Para el cumplimiento de los requisitos correspondientes a todos los procedimientos administrativos, comunes o especiales, las entidades están obligadas a recibir los siguientes documentos e informaciones en vez de la documentación oficial, a la cual reemplazan con el mismo mérito probatorio:

49.1.1 Copias simples en reemplazo de documentos originales o copias legalizadas notarialmente de tales documentos, acompañadas de declaración jurada del administrado acerca de su autenticidad. Las copias simples serán aceptadas, estén o no certificadas por notarios, funcionarios o servidores públicos en el ejercicio de sus funciones y tendrán el mismo valor que los documentos originales para el cumplimiento de los requisitos correspondientes a la tramitación de procedimientos administrativos seguidos ante cualquier entidad.

49.1.2 Traducciones simples con la indicación y suscripción de quien oficie de traductor debidamente identificado, en lugar de traducciones oficiales.

49.1.3 Las expresiones escritas del administrado contenidas en declaraciones con carácter jurado mediante las cuales afirman su situación o estado favorable, así como la existencia, veracidad, vigencia en reemplazo de la información o documentación prohibida de solicitar.

49.1.4 Instrumentos privados, boletas notariales o copias simples de las escrituras públicas, en vez de instrumentos públicos de cualquier naturaleza, o testimonios notariales, respectivamente.

49.1.5 Constancias originales suscritas por profesionales independientes debidamente identificados en reemplazo de certificaciones oficiales acerca de las condiciones especiales del administrado o de sus intereses cuya apreciación requiera especiales actitudes técnicas o profesionales para reconocerlas, tales como certificados de salud o planos arquitectónicos, entre otros. Se tratará de profesionales colegiados sólo cuando la norma que regula los requisitos del procedimiento así lo exija.

49.1.6 Copias fotostáticas de formatos oficiales o una reproducción particular de ellos elaborada por el administrador respetando integralmente la estructura de los definidos por la autoridad, en sustitución de los formularios oficiales aprobados por la propia entidad para el suministro de datos.

49.2 La presentación y admisión de los sucedáneos documentales, se hace al amparo del principio de presunción de veracidad y conlleva la realización obligatoria de acciones de fiscalización posterior a cargo de dichas entidades, con la consecuente aplicación de las sanciones previstas en el numeral 34.3 del artículo 34 si se comprueba el fraude o falsedad.

49.3 Lo dispuesto en el presente artículo es aplicable aun cuando una norma expresa disponga la presentación de documentos originales.

49.4 Las disposiciones contenidas en este artículo no limitan el derecho del administrado a presentar la documentación prohibida de exigir, en caso de ser considerado conveniente a su derecho.

49.5 Mediante Decreto Supremo refrendado por el Presidente del Consejo de Ministros y del sector competente se puede ampliar la relación de documentos originales que pueden ser reemplazados por sucedáneos.

(Texto según el artículo 41 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1272)

Artículo 50.- Validez de actos administrativos de otras entidades y suspensión del procedimiento
================================================================================================

Salvo norma especial, en la tramitación de procedimientos administrativos las entidades no pueden cuestionar la validez de actos administrativos emitidos por otras entidades que son presentados para dar cumplimiento a los requisitos de los procedimientos administrativos a su cargo. Tampoco pueden suspender la tramitación de los procedimientos a la espera de resoluciones o información provenientes de otra entidad.

(Artículo incorporado por el artículo 4 del Decreto Legislativo Nº 1272)

Artículo 51.- Presunción de veracidad
=====================================

51.1 Todas las declaraciones juradas, los documentos sucedáneos presentados y la información incluida en los escritos y formularios que presenten los administrados para la realización de procedimientos administrativos, se presumen verificados por quien hace uso de ellos, respecto a su propia situación, así como de contenido veraz para fines administrativos, salvo prueba en contrario. En caso de documentos emitidos por autoridades gubernamentales o por terceros, el administrado puede acreditar su debida diligencia en realizar previamente a su presentación las verificaciones correspondientes y razonables.

51.2 En caso de las traducciones de parte, así como los informes o constancias profesionales o técnicas presentadas como sucedáneos de documentación oficial, dicha responsabilidad alcanza solidariamente a quien los presenta y a los que los hayan expedido.

(Texto según el artículo 42 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 52.- Valor de documentos públicos y privados
=====================================================

52.1 Son considerados documentos públicos aquellos emitidos válidamente por los órganos de las entidades.

52.2 La copia de cualquier documento público goza de la misma validez y eficacia que éstos, siempre que exista constancia de que es auténtico.

52.3 La copia del documento privado cuya autenticidad ha sido certificada por el fedatario, tiene validez y eficacia plena, exclusivamente en el ámbito de actividad de la entidad que la autentica.

(Texto según el artículo 43 de la Ley Nº 27444)

Artículo 53.- Derecho de tramitación
====================================

53.1 Procede establecer derechos de tramitación en los procedimientos administrativos, cuando su tramitación implique para la entidad la prestación de un servicio específico e individualizable a favor del administrado, o en función del costo derivado de las actividades dirigidas a analizar lo solicitado; salvo en los casos en que existan tributos destinados a financiar directamente las actividades de la entidad. Dicho costo incluye los gastos de operación y mantenimiento de la infraestructura asociada a cada procedimiento.

53.2 Son condiciones para la procedencia de este cobro que los derechos de tramitación hayan sido determinados conforme a la metodología vigente, y que estén consignados en su vigente Texto Único de Procedimientos Administrativos. Para el caso de las entidades del Poder Ejecutivo se debe contar, además, con el refrendo del Ministerio de Economía y Finanzas.

(Texto según el numeral 44.2 del artículo 44 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1452)

53.3 No procede establecer cobros por derecho de tramitación para procedimientos iniciados de oficio, ni en aquellos en los que son ejercidos el derecho de petición graciable, regulado en el artículo 123, o el de denuncia ante la entidad por infracciones funcionales de sus propios funcionarios o que deban ser conocidas por los Órganos de Control Institucional, para lo cual cada entidad debe establecer el procedimiento correspondiente.

(Texto según el numeral 44.3 del artículo 44 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1272)

53.4 No pueden dividirse los procedimientos ni establecerse cobro por etapas.

53.5 La entidad está obligada a reducir los derechos de tramitación en los procedimientos administrativos si, como producto de su tramitación, se hubieren generado excedentes económicos en el ejercido anterior.

53.6 Mediante decreto supremo refrendado por el Presidente del Consejo de Ministros y el Ministro de Economía y Finanzas se precisa los criterios, procedimientos y metodologías para la determinación de los costos de los procedimientos, y servicios administrativos que brinda la administración y para la fijación de los derechos de tramitación. La aplicación de dichos criterios, procedimientos y metodologías es obligatoria para la determinación de costos de los procedimientos administrativos y servicios prestados en exclusividad para todas las entidades públicas en los procesos de elaboración o modificación del Texto Único de Procedimientos Administrativos de cada entidad. La entidad puede aprobar derechos de tramitación menores a los que resulten de la aplicación de los criterios, procedimientos y metodologías aprobados según el presente artículo.

(Texto según el numeral 44.6 del artículo 44 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

53.7 Mediante Decreto Supremo refrendado por el Presidente del Consejo de Ministros y el Ministro de Economía y Finanzas, siguiendo lo previsto en el numeral anterior, se pueden aprobar los derechos de tramitación para los procedimientos estandarizados, que son de obligatorio cumplimiento por parte de las entidades a partir de su publicación en el Diario Oficial, sin necesidad de realizar actualización del Texto Único de Procedimientos Administrativos. Sin perjuicio de lo anterior, las entidades están obligadas a incorporar el monto del derecho de tramitación en sus Texto Único de Procedimientos Administrativos dentro del plazo máximo de cinco (5) días hábiles, sin requerir un trámite de aprobación de derechos de tramitación, ni su ratificación.

(Texto modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 54.- Límite de los derechos de tramitación
===================================================

54.1 El monto del derecho de tramitación es determinado en función al importe del costo que su ejecución genera para la entidad por el servicio prestado durante toda su tramitación y, en su caso, por el costo real de producción de documentos que expida la entidad. Su monto es sustentado por el servidor a cargo de la oficina de administración de cada entidad.

Para que el costo sea superior a una (1) UIT, se requiere autorización del Ministerio de Economía y Finanzas conforme a los lineamientos para la elaboración y aprobación del Texto Único de Procedimientos Administrativos aprobados por Resolución de Secretaria de Gestión Pública. Dicha autorización no es aplicable en los casos en que la Presidencia del Consejo de Ministros haya aprobado derechos de tramitación para los procedimientos estandarizados.

(Texto según el numeral 45.1 del artículo 45 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1452)

54.2 Las entidades no pueden establecer pagos diferenciados para dar preferencia o tratamiento especial a una solicitud distinguiéndola de las demás de su mismo tipo, ni discriminar en función al tipo de administrado que siga el procedimiento.

(Texto según el artículo 45 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 55.- Cancelación de los derechos de tramitación
========================================================

La forma de cancelación de los derechos de tramitación es establecida en el TUPA institucional, debiendo tender a que el pago a favor de la entidad pueda ser realizado mediante cualquier forma dineraria que permita su constatación, incluyendo abonos en cuentas bancarias o transferencias electrónicas de fondos.

(Texto según el artículo 46 de la Ley Nº 27444)

Artículo 56.- Reembolso de gastos administrativos
=================================================

56.1 Solo procede el reembolso de gastos administrativos cuando una ley expresamente lo autoriza.

Son gastos administrativos aquellos ocasionados por actuaciones específicas solicitados por el administrado dentro del procedimiento. Se solicita una vez iniciado el procedimiento administrativo y es de cargo del administrado que haya solicitado la actuación o de todos los administrados, si el asunto fuera de interés común; teniendo derecho a constatar y, en su caso, a observar, el sustento de los gastos a reembolsar.

56.2 En el caso de los procedimientos administrativos trilaterales, las entidades podrán ordenar en el acto administrativo que causa estado la condena de costas y costos por la interposición de recursos administrativos maliciosos o temerarios. Se entiende por recurso malicioso o temerario aquel carente de todo sustento de hecho y de derecho, de manera que por la ostensible falta de rigor en su fundamentación se evidencia la intención de mala fe del administrado. Para ello, se debe acreditar el conocimiento objetivo del administrado de ocasionar un perjuicio. Los lineamientos para la aplicación de este numeral se aprobarán mediante Decreto Supremo refrendado por el Presidente de la Presidencia del Consejo de Ministros.

(Texto según el artículo 47 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 57.- Cumplimiento de las normas del presente capítulo
==============================================================

57.1 La Presidencia del Consejo de Ministros, como entidad rectora, es la máxima autoridad técnico normativa del Sistema de Modernización de la Gestión Pública y tiene a su cargo garantizar el cumplimiento de las normas establecidas en el presente capítulo en todas las entidades de la administración pública, sin perjuicio de las facultades atribuidas a la Comisión de Eliminación de Barreras Burocráticas del Instituto Nacional de Defensa de la Competencia y de Protección de la Propiedad Intelectual para conocer y resolver denuncias que los ciudadanos o agentes económicos le formulen sobre el tema.

57.2 La Presidencia del Consejo de Ministros tiene las siguientes competencias:

1. Dictar Directivas, metodologías y lineamientos técnico normativos en las materias de su competencia, incluyendo aquellas referidas a la creación de procedimientos administrativos y servicios prestados en exclusividad.

2. Emitir opinión vinculante sobre el alcance e interpretación de las normas de simplificación administrativa incluyendo la presente Ley. En el caso de los Texto Único de Procedimientos Administrativos de los Ministerios y Organismos Públicos, emitir opinión previa favorable a su aprobación.

3. Asesorar a las entidades en materia de simplificación administrativa y evaluar de manera permanente los procesos de simplificación administrativa al interior de las entidades, para lo cual podrá solicitar toda la información que requiera de éstas.

4. Supervisar y velar el cumplimiento de las normas de la presente Ley, salvo lo relativo a la determinación de los derechos de tramitación.

5. Supervisar que las entidades cumplan con aprobar sus Texto Único de Procedimientos Administrativos conforme a la normativa aplicable.

6. Realizar las gestiones del caso conducentes a hacer efectiva la responsabilidad de los funcionarios por el incumplimiento de las normas del presente Capítulo, para lo cual cuenta con legitimidad para accionar ante las diversas entidades de la administración pública.

7. Establecer los mecanismos para la recepción de quejas y otros mecanismos de participación de la ciudadanía. Cuando dichas quejas se refieran a asuntos de la competencia de la Comisión de Eliminación de Barreras Burocráticas, se inhibirá de conocerlas y las remitirá directamente a ésta.

8. Detectar los incumplimientos a las normas de la presente Ley y ordenar las modificaciones normativas pertinentes, otorgando a las entidades un plazo perentorio para la subsanación.

9. En caso de no producirse la subsanación, la Presidencia del Consejo de Ministros entrega un informe a la Comisión de Eliminación de Barreras Burocráticas del INDECOPI, a fin de que inicie de oficio un procedimiento de eliminación de barreras burocráticas, sin perjuicio de la aplicación de lo previsto en el artículo 261.

Asimismo, la Comisión de Eliminación de Barreras Burocráticas del INDECOPI tiene la competencia de fiscalizar:

a. Que las entidades cumplan con aplicar los procedimientos estandarizados e incorporarlos en sus Textos Únicos de Procedimientos Administrativos.

b. Que las entidades cumplan con las normas de simplificación administrativa en la tramitación de sus procedimientos administrativos y servicios prestados en exclusividad.

10. Solicitar a la Secretaría Técnica de la Comisión de Barreras Burocráticas el inicio de un procedimiento de oficio en materia de eliminación de barreras burocráticas contenidas en disposiciones administrativas que regulen el ejercicio de actividades económicas significativas para el desarrollo del país.

11. Otras previstas en la presente Ley y las que señalen los dispositivos legales correspondientes.

(Texto según el artículo 48 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1452)

Artículo 58.- Régimen de entidades sin Texto Único de Procedimientos Administrativos vigente
============================================================================================

58.1 Cuando la entidad no cumpla con publicar su Texto Único de Procedimientos Administrativos, o lo publique omitiendo procedimientos, los administrados, sin perjuicio de hacer efectiva la responsabilidad de la autoridad infractora, quedan sujetos al siguiente régimen:

1. Respecto de los procedimientos administrativos que corresponde ser aprobados automáticamente o que se encuentran sujetos a silencio administrativo positivo, los administrados quedan liberados de la exigencia de iniciar ese procedimiento para obtener la autorización previa, para realizar su actividad profesional, social, económica o laboral, sin ser pasibles de sanciones por el libre desarrollo de tales actividades. La suspensión de esta prerrogativa de la autoridad concluye a partir del día siguiente de la publicación del TUPA, sin efecto retroactivo.

Los procedimientos administrativos sujetos a silencio administrativo negativo siguen el régimen previsto en la norma de creación o modificación del respectivo procedimiento administrativo.

(Texto según el artículo 49 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1452)

2. Respecto de las demás materias sujetas a procedimiento de evaluación previa, se sigue el régimen previsto en cada caso por este Capítulo.

58.2 El incumplimiento de las obligaciones de aprobar y publicar los Texto Único de Procedimientos, genera las siguientes consecuencias:

1. Para la entidad, la suspensión de sus facultades de exigir al administrado la tramitación del procedimiento administrativo, la presentación de requisitos o el pago del derecho de tramitación, para el desarrollo de sus actividades.

2. Para los funcionarios responsables de la aplicación de las disposiciones de la presente Ley y las normas reglamentarias respectivas, constituye una falta disciplinaria grave.

(Texto según el artículo 49 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 59.- Tercerización de actividades
==========================================

Todas las actividades vinculadas a las funciones de fiscalización, los procedimientos administrativos y servicios prestados en exclusividad distintas a la emisión de los actos administrativos o cualquier resolución pueden tercerizarse salvo disposición distinta de la ley. Mediante Decreto Supremo refrendado por la Presidencia del Consejo de Ministros se establecen las disposiciones necesarias para la aplicación de esta modalidad.

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1452)

Artículo 60.- Rol de la Contraloría General y de los órganos de control interno
===============================================================================

60.1 Corresponde a la Contraloría General de la República y a los órganos de control interno de las entidades, en el marco de la Ley N 27785, Ley Orgánica del Sistema Nacional de Control y de la Contraloría General de la República, verificar de oficio que las entidades y sus funcionarios y servidores públicos cumplan con las obligaciones que se establecen en el Capítulo I, Disposiciones Generales, del Título, II Procedimiento Administrativo, de la Ley Nº 27444, Ley de Procedimiento Administrativo General.

60.2 Los administrados podrán presentar denuncias ante los órganos de control interno de las entidades, que forman parte del Sistema Nacional de Control, o directamente ante la Contraloría General de la República, contra los funcionarios o servidores públicos que incumplan cualquiera de las obligaciones a que se refiere el párrafo anterior.

60.3 Es obligación de los órganos de control interno de las entidades o de la Contraloría General de la República que conocen de las denuncias informar a los denunciantes sobre el trámite de las mismas y sobre las acciones que se desarrollen, o las decisiones que se adopten, como resultado de las denuncias en relación a las irregularidades o incumplimientos que son objeto de denuncia.

60.4 El jefe o responsable del órgano de control interno tiene la obligación de realizar trimestralmente un reporte, que deberá remitir al titular de la entidad para que disponga que en un plazo no mayor de 5 días hábiles se publique en el respectivo portal web de transparencia institucional, en el que dará cuenta de las acciones realizadas, o de las decisiones adoptadas, en relación a las denuncias que reciba contra los funcionarios o servidores públicos que incumplan las obligaciones a que se refiere el primer párrafo de este dispositivo.

(Artículo incorporado por el artículo 4 del Decreto Legislativo Nº 1272)

