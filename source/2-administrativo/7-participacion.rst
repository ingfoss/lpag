===============================================
CAPÍTULO VII Participación de los administrados
===============================================

Artículo 192.- Administración abierta
=====================================

Además de los medios de acceso a la participación en los asuntos públicos establecidos por otras normas, en la instrucción de los procedimientos administrativos las entidades se rigen por las disposiciones de este Capítulo sobre la audiencia a los administrados y el período de información pública.

(Texto según el artículo 181 de la Ley Nº 27444)

Artículo 193.- Audiencia pública
================================

193.1 Las normas administrativas prevén la convocatoria a una audiencia pública, como formalidad esencial para la participación efectiva de terceros, cuando el acto al que conduzca el procedimiento administrativo sea susceptible de afectar derechos o intereses cuya titularidad corresponda a personas indeterminadas, tales como en materia medio ambiental, ahorro público, valores culturales, históricos, derechos del consumidor, planeamiento urbano y zonificación; o cuando el pronunciamiento sobre autorizaciones, licencias o permisos que el acto habilite incida directamente sobre servicios públicos.

193.2 En la audiencia pública cualquier tercero, sin necesidad de acreditar legitimación especial está habilitado para presentar información verificada, para requerir el análisis de nuevas pruebas, así como expresar su opinión sobre las cuestiones que constituyan el objeto del procedimiento o sobre la evidencia actuada. No procede formular interpelaciones a la autoridad en la audiencia.

193.3 La omisión de realización de la audiencia pública acarrea la nulidad del acto administrativo final que se dicte.

193.4 El vencimiento del plazo previsto en el artículo 153, sin que se haya llevado a cabo la audiencia pública, determina la operatividad del silencio administrativo negativo, sin perjuicio de la responsabilidad de las autoridades obligadas a su convocatoria.

(Texto según el artículo 182 de la Ley Nº 27444)

Artículo 194.- Convocatoria a audiencia pública
===============================================

La convocatoria a audiencia pública debe publicarse en el Diario Oficial o en uno de los medios de comunicación de mayor difusión local, según la naturaleza del asunto, con una anticipación no menor de tres (3) días a su realización, debiendo indicar: la autoridad convocante, su objeto, el día, lugar y hora de realización, los plazos para inscripción de participantes, el domicilio y teléfono de la entidad convocante, dónde se puede realizar la inscripción, se puede acceder a mayor información del asunto, o presentar alegatos, impugnaciones y opiniones.

(Texto según el artículo 183 de la Ley Nº 27444)

Artículo 195.- Desarrollo y efectos de la audiencia pública
===========================================================

195.1 La comparecencia a la audiencia no otorga, por sí misma, la condición de participante en el procedimiento.

195.2 La no asistencia a la audiencia no impide a los legitimados en el procedimiento como interesados, a presentar alegatos, o recursos contra la resolución.

195.3 Las informaciones y opiniones manifestadas durante la audiencia pública, son registradas sin generar debate, y poseen carácter consultivo y no vinculante para la entidad.

195.4 La autoridad instructora debe explicitar, en los fundamentos de su decisión, de qué manera ha tomado en cuenta las opiniones de la ciudadanía y, en su caso, las razones para su desestimación.

(Texto según el artículo 184 de la Ley Nº 27444)

Artículo 196.- Período de información pública
=============================================

196.1 Cuando sea materia de decisión de la autoridad, cualquier aspecto de interés general distinto a los previstos en el artículo anterior donde se aprecie objetivamente que la participación de terceros no determinados pueda coadyuvar a la comprobación de cualquier estado, información o de alguna exigencia legal no evidenciada en el expediente por la autoridad, el instructor abre un período no menor de tres ni mayor de cinco días hábiles para recibir -por los medios más amplios posibles- sus manifestaciones sobre el asunto, antes de resolver el procedimiento.

196.2 El período de información pública corresponde ser convocado particularmente antes de aprobar normas administrativas que afecten derechos e intereses ciudadanos, o para resolver acerca del otorgamiento de licencias o autorizaciones para ejercer actividades de interés general, y para designar funcionarios en cargos principales de las entidades, o incluso tratándose de cualquier cargo cuando se exija como condición expresa poseer conducta intachable o cualquier circunstancia análoga.

196.3 La convocatoria, desarrollo y consecuencias del período de información pública se sigue en lo no previsto en este Capítulo, en lo aplicable, por las normas de audiencia pública.

(Texto según el artículo 185 de la Ley Nº 27444)

