TÍTULO III De la Revisión de los Actos en Vía Administrativa

=============================
CAPÍTULO I Revisión de Oficio
=============================

Artículo 212.- Rectificación de errores
=======================================

212.1 Los errores material o aritmético en los actos administrativos pueden ser rectificados con efecto retroactivo, en cualquier momento, de oficio o a instancia de los administrados, siempre que no se altere lo sustancial de su contenido ni el sentido de la decisión.

212.2 La rectificación adopta las formas y modalidades de comunicación o publicación que corresponda para el acto original.

(Texto según el artículo 201 de la Ley Nº 27444)

Artículo 213.- Nulidad de oficio
================================

213.1 En cualquiera de los casos enumerados en el artículo 10, puede declararse de oficio la nulidad de los actos administrativos, aun cuando hayan quedado firmes, siempre que agravien el interés público o lesionen derechos fundamentales.

213.2 La nulidad de oficio solo puede ser declarada por el funcionario jerárquico superior al que expidió el acto que se invalida. Si se tratara de un acto emitido por una autoridad que no está sometida a subordinación jerárquica, la nulidad es declarada por resolución del mismo funcionario.

Además de declarar la nulidad, la autoridad puede resolver sobre el fondo del asunto de contarse con los elementos suficientes para ello. En este caso, este extremo sólo puede ser objeto de reconsideración. Cuando no sea posible pronunciarse sobre el fondo del asunto, se dispone la reposición del procedimiento al momento en que el vicio se produjo.

En caso de declaración de nulidad de oficio de un acto administrativo favorable al administrado, la autoridad, previamente al pronunciamiento, le corre traslado, otorgándole un plazo no menor de cinco (5) días para ejercer su derecho de defensa.

213.3. La facultad para declarar la nulidad de oficio de los actos administrativos prescribe en el plazo de dos (2) años, contado a partir de la fecha en que hayan quedado consentidos, o contado a partir de la notificación a la autoridad administrativa de la sentencia penal condenatoria firme, en lo referido a la nulidad de los actos previstos en el numeral 4 del artículo 10.

(Texto según el numeral 202.3 del artículo 202 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1452)

213.4 En caso de que haya prescrito el plazo previsto en el numeral anterior, sólo procede demandar la nulidad ante el Poder Judicial vía el proceso contencioso administrativo, siempre que la demanda se interponga dentro de los tres (3) años siguientes a contar desde la fecha en que prescribió la facultad para declarar la nulidad en sede administrativa.

213.5. Los actos administrativos emitidos por consejos o tribunales regidos por leyes especiales, competentes para resolver controversias en última instancia administrativa, sólo pueden ser objeto de declaración de nulidad de oficio en sede administrativa por el propio consejo o tribunal con el acuerdo unánime de sus miembros. Esta atribución sólo puede ejercerse dentro del plazo de dos (2) años contados desde la fecha en que el acto haya quedado consentido. También procede que el titular de la Entidad demande su nulidad en la vía de proceso contencioso administrativo, siempre que la demanda se interponga dentro de los tres años siguientes de notificada la resolución emitida por el consejo o tribunal.

(Texto según el numeral 202.5 del artículo 202 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1452)

Artículo 214.- Revocación
=========================

214.1 Cabe la revocación de actos administrativos, con efectos a futuro, en cualquiera de los siguientes casos:

214.1.1 Cuando la facultad revocatoria haya sido expresamente establecida por una norma con rango legal y siempre que se cumplan los requisitos previstos en dicha norma.

214.1.2 Cuando sobrevenga la desaparición de las condiciones exigidas legalmente para la emisión del acto administrativo cuya permanencia sea indispensable para la existencia de la relación jurídica creada.

214.1.3 Cuando apreciando elementos de juicio sobrevinientes se favorezca legalmente a los destinatarios del acto y siempre que no se genere perjuicios a terceros.

214.1.4 Cuando se trate de un acto contrario al ordenamiento jurídico que cause agravio o perjudique la situación jurídica del administrado, siempre que no lesione derechos de terceros ni afecte el interés público.

La revocación prevista en este numeral solo puede ser declarada por la más alta autoridad de la entidad competente, previa oportunidad a los posibles afectados otorgándole un plazo no menor de cinco (5) días para presentar sus alegatos y evidencias en su favor.

214.2 Los actos administrativos declarativos o constitutivos de derechos o intereses legítimos no pueden ser revocados, modificados o sustituidos de oficio por razones de oportunidad, mérito o conveniencia.

(Texto según el artículo 203 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 215.- lrrevisabilidad de actos judicialmente confirmados
=================================================================

No serán en ningún caso revisables en sede administrativa los actos que hayan sido objeto de confirmación por sentencia judicial firme.

(Texto según el artículo 204 de la Ley Nº 27444)

Artículo 216.- Indemnización por revocación
===========================================

216.1 Cuando la revocación origine perjuicio económico al administrado, la resolución que la decida deberá contemplar lo conveniente para efectuar la indemnización correspondiente en sede administrativa.

216.2 Los actos incursos en causal para su revocación o nulidad de oficio, pero cuyos efectos hayan caducado o agotado, serán materia de indemnización en sede judicial, dispuesta cuando quede firme administrativamente su revocación o anulación.

(Texto según el artículo 205 de la Ley Nº 27444)

