==========================================
DISPOSICIONES COMPLEMENTARIAS DEROGATORIAS
==========================================

Primera.- Derogación genérica
=============================

Esta Ley es de orden público y deroga todas las disposiciones legales o administrativas, de igual o inferior rango, que se le opongan o contradigan, regulando procedimientos administrativos de índole general, aquellos cuya especialidad no resulte justificada por la materia que rijan, así como por absorción aquellas disposiciones que presentan idéntico contenido que algún precepto de esta Ley.

(Texto según la sección de las Disposiciones Complementarias y Finales de la Ley Nº 27444)

Segunda.- Derogación expresa
============================

Particularmente quedan derogadas expresamente a partir de la vigencia de la presente Ley, las siguientes normas:

1. El Decreto Supremo Nº 006-67-SC, la Ley Nº 26111, el Texto Único Ordenado de la Ley de Normas Generales de Procedimientos Administrativos, aprobado por Decreto Supremo Nº 002-94-JUS y sus normas modificatorias, complementarias, sustitutorias y reglamentarias;

2. Ley Nº 25035, denominada Ley de Simplificación Administrativa, y sus normas modificatorias, complementarias, sustitutorias y reglamentarias;

3. Título IV del Decreto Legislativo Nº 757, denominado Ley Marco para el Crecimiento de la Inversión Privada, y sus normas modificatorias, complementarias, sustitutorias y reglamentarias;

4. Sexta Disposición Complementaria y Transitoria de la Ley Nº 26979, denominada Ley de Procedimiento de Ejecución Coactiva.

(Texto según la sección de las Disposiciones Complementarias y Finales de la Ley Nº 27444)

Tercera.- A partir de la vigencia de la presente Ley, quedan derogadas expresamente las siguientes normas:
==========================================================================================================

1) La Ley Nº 29060, Ley del Silencio Administrativo.

2) Los artículos 210 y 240 de la Ley 27444, Ley del Procedimiento Administrativo General.

3) El artículo 279 del Capítulo XIX del Título Décimo Primero de la Ley General de Minería, aprobado por el Decreto Legislativo Nº 109, recogido en el artículo 161 del Capítulo XVII del Título Décimo Segundo del Texto Único Ordenado de la Ley General de Minería, aprobado por el Decreto Supremo Nº 014-92-EM, siendo de aplicación las disposiciones de la presente Ley.

(Texto según la Disposición Complementaria Derogatoria del Decreto Legislativo Nº 1272)


